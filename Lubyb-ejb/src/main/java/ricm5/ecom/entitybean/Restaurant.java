package ricm5.ecom.entitybean;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Restaurant implements Serializable {
    private static final long serialVersionUID = 1L;
    public enum Status {VALIDATED, PENDING, UNKNOWN};
     
    // L'id d'un restaurant est son numero siret (composé de 14 chiffres et unique)
    // En string pour prendre en compte les 00 en début
    @Id
    private String siret;

    private String name;
    private String adresse;
    @ManyToOne
    private Utilisateur responsable;
    @Enumerated(EnumType.STRING)
    private Status status;
    private double solde;
    //Pourcentage reversé ? (valeur entre 0 et 1, par default 0.05 = 5% de chaque commande)
    private double taux;

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public double getTaux() {
        return taux;
    }

    public double getSolde() {
        return solde;
    }

    public void setSolde(double solde) {
        this.solde = solde;
    }
    

    public void setTaux(double taux) {
        this.taux = taux;
    }
    
    public String getSiret() {
        return siret;
    }

    public String getName() {
        return name;
    }

    public String getAdresse() {
        return adresse;
    }

    public Utilisateur getResponsable() {
        return responsable;
    }

    public Status getStatus() {
        return status;
    }

    public void setSiret(String siret) {
        this.siret = siret;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public void setResponsable(Utilisateur responsable) {
        this.responsable = responsable;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (siret != null ? siret.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Restaurant)) {
            return false;
        }
        Restaurant other = (Restaurant) object;
        if ((this.siret == null && other.siret != null) || (this.siret != null && !this.siret.equals(other.siret))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ricm5.ecom.entitybean.Restaurant[ siret=" + siret + " ]";
    }
    
}
