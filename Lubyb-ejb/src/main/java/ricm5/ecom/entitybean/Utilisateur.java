package ricm5.ecom.entitybean;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;

@Entity
public class Utilisateur implements Serializable {
    public enum Role {
        ADMIN, CLIENT, RESTAURATEUR
    }
    private static final long serialVersionUID = 1L;
    
    @Id
    private String login;
    private String nom ;
    private String prenom ; 
    private String email;

    
    private byte[] encryptedPassword;
    // for user identification
    private byte[] salt; 
    
    @Enumerated(EnumType.STRING)
    private Role roleUser;
    private String adresseFact ;
    private String adresseLiv ;
    private String telephone ;

    public boolean isAdmin(){
        return (roleUser == Role.ADMIN);
    }
    
    public boolean isClient(){
        return (roleUser == Role.CLIENT);
    }
    
    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }


    public byte[] getEncryptedPassword() {
        return encryptedPassword;
    }

    public byte[] getSalt() {
        return salt;
    }
    
    public String getNom() {
        return nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public String getRoleUser() {
        return roleUser.toString();
    }

    public String getEmail() {
        return email;
    }

    
    //////////////// SETTERS ////////////////////
    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public void setEncryptedPassword(byte[] encryptedPassword) {
        this.encryptedPassword = encryptedPassword;
    }

    public void setSalt(byte[] salt) {
        this.salt = salt;
    }
    

    public void setRoleUser(Role roleUser) {
        this.roleUser = roleUser;
    }
    
    public void setEmail(String email) {
        this.email = email;
    }
    
    public String getAdresseFact() {
        return adresseFact;
    }

    public String getAdresseLiv() {
        return adresseLiv;
    }


    public String getTelephone() {
        return telephone;
    }
    

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }
    
    public void setAdresseFact(String adresseFact) {
        this.adresseFact = adresseFact;
    }

    public void setAdresseLiv(String adresseLiv) {
        this.adresseLiv = adresseLiv;
    }
    
}
