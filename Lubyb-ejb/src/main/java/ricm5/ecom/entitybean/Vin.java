package ricm5.ecom.entitybean;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Vin implements Serializable {
    private static final long serialVersionUID = 1L;
    public enum Couleur {ROUGE, ROSE, BLANC, JAUNE, UNKNOWN};
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
    private String nom;
    private double prix;
    private int annee;
    @ManyToOne
    private Region region;
    private int stock;
    @Enumerated(EnumType.STRING)
    private Couleur type;
    private String imageUri;

    public void setImageUri(String imageUri) {
        this.imageUri = imageUri;
    }

    public String getImageUri() {
        return imageUri;
    }
    
    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getId() {
        return id;
    }

    public String getNom() {
        return nom;
    }

    public double getPrix() {
        return prix;
    }

    public int getAnnee() {
        return annee;
    }

    public Region getRegion() {
        return region;
    }

    public int getStock() {
        return stock;
    }

    public Couleur getType() {
        return type;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setPrix(double prix) {
        this.prix = prix;
    }

    public void setAnnee(int annee) {
        this.annee = annee;
    }

    public void setRegion(Region region) {
        this.region = region;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public void setType(Couleur type) {
        this.type = type;
    }
    
    public void setType(String type) {
        if(type.equals("rouge") || type.equals("Rouge")){
            this.type = Couleur.ROUGE;            
        }
        else if(type.equals("blanc") || type.equals("Blanc")){
            this.type = Couleur.BLANC;
        }else if(type.equals("rose") || type.equals("Rose") || type.equals("Rosé")){
            this.type = Couleur.ROSE;
        }
        else if(type.equals("jaune") || type.equals("Jaune")){
            this.type = Couleur.JAUNE;
        }
        else {
            this.type = Couleur.UNKNOWN;
        }
    }
   
    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Vin)) {
            return false;
        }
        Vin other = (Vin) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ricm5.ecom.Wine[ id=" + id + " ]";
    }
    
    public String getDescription(){
        return ("" + this.getRegion().toString() + " - " + this.getAnnee() + " - " + this.getType().toString().toLowerCase()); 
    }
}
