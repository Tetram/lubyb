package ricm5.ecom.entitybean;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;


@Entity
public class Commande implements Serializable {
    private static final long serialVersionUID = 1L;
    public enum Status {PENDING, READY, DELIVERED, UNKNOWN};
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date dateCommande;
    
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date dateToDeliver;
    
    @ManyToOne
    private Utilisateur client;
    @ManyToOne
    private Restaurant restaurant;
    private String nomClient;
    private String prenomClient;
    private HashMap produits;
    private double montant;
    // montant a verser au restaurateur
    private double droitBouchon;
    @Enumerated(EnumType.STRING)
    private Status status;

    public Date getDateCommande() {
        return dateCommande;
    }

    public void setDateCommande(Date dateCommande) {
        this.dateCommande = dateCommande;
    }
    
    public Utilisateur getClient() {
        return client;
    }

    public void setClient(Utilisateur client) {
        this.client = client;
    }

    public double getMontant() {
        return montant;
    }

    public void setMontant(double montant) {
        this.montant = montant;
    }



    public double getDroitBouchon() {
        return droitBouchon;
    }

    public void setDroitBouchon(double droitBouchon) {
        this.droitBouchon = droitBouchon;
    }
    

    public Long getId() {
        return id;
    }
    
    public HashMap getProduits() {
        return produits;
    }
    
    public String getNomClient() {
        return nomClient;
    }
    
    public Restaurant getRestaurant() {
        return restaurant;
    }

    public void setRestaurant(Restaurant restaurant) {
        this.restaurant = restaurant;
    }
        

    public void setNomClient(String nomClient) {
        this.nomClient = nomClient;
    }

    public String getPrenomClient() {
        return prenomClient;
    }

    public void setPrenomClient(String prenomClient) {
        this.prenomClient = prenomClient;
    }

    public void setId(Long id) {
        this.id = id;
    }
    
    public void setProduits(HashMap contenu) {
        this.produits = contenu;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public void setDateToDeliver(Date dateToDeliver) {
        this.dateToDeliver = dateToDeliver;
    }

    public Date getDateToDeliver() {
        return dateToDeliver;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Commande)) {
            return false;
        }
        Commande other = (Commande) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "ricm5.ecom.entitybean.Commande[ id=" + id + " ]";
    }
    
}
