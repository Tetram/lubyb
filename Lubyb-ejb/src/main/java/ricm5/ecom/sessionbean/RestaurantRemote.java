package ricm5.ecom.sessionbean;

import java.util.List;
import javax.ejb.Remote;
import ricm5.ecom.entitybean.Restaurant;
import ricm5.ecom.entitybean.Utilisateur;

@Remote
public interface RestaurantRemote {
    void add(Restaurant v);
    
    void delete(String ref);
    void create(Restaurant entity);

    void edit(Restaurant entity);

    void remove(Restaurant entity);

    Restaurant find(Object id);

    List<Restaurant> findAll();

    List<Restaurant> findRange(int[] range);

    List<Restaurant> findByStatus(Restaurant.Status status);
    
    List<Restaurant> findByUserInCharge(Utilisateur user);
    
    int count();

    void addSolde(String idRestaurant, double solde);
}
