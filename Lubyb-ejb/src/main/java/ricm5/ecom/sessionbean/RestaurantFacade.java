package ricm5.ecom.sessionbean;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityExistsException;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import ricm5.ecom.entitybean.Restaurant;
import ricm5.ecom.entitybean.Utilisateur;

@Stateless
public class RestaurantFacade extends AbstractFacade<Restaurant> implements RestaurantRemote{
    
    @PersistenceContext(unitName = "ricm5.ecom_Lubyb-ejb_ejb_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public RestaurantFacade() {
        super(Restaurant.class);
    }
    
    @Override
    public void add(Restaurant r){
        try{
            if(em.find(Restaurant.class, r.getSiret()) != null){
                em.merge(r);
            }else{
                em.persist(r);
            }
        }catch(EntityExistsException e){
            
        }
    }
    
    @Override
    public void delete(String siret) {
        try{
            Restaurant v = getEntityManager().find(Restaurant.class, siret);
            getEntityManager().remove(v);
        }catch(Exception e){
            // Restaurant introuvble...
        }
    }
    
    @Override
    public List<Restaurant> findByStatus(Restaurant.Status status){
        Query q = em.createQuery("select r from Restaurant r where r.status = :stat");
        q.setParameter("stat", status);
        return q.getResultList();
    }

    @Override
    public List<Restaurant> findByUserInCharge(Utilisateur user) {
        Query q = em.createQuery("select r from Restaurant r where r.responsable = :user");
        q.setParameter("user", user);
        return q.getResultList();
    }
    
    @Override
    public void addSolde(String idRestaurant, double solde) {
        Restaurant resto = em.find(Restaurant.class, idRestaurant);
        if(resto != null){
            Double oldSolde = resto.getSolde();
            resto.setSolde(oldSolde + solde);
            em.merge(resto);
        }
    }
}
