package ricm5.ecom.sessionbean;

import javax.ejb.Stateless;
import javax.persistence.EntityExistsException;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import ricm5.ecom.entitybean.Utilisateur;

@Stateless
public class UserFacade extends AbstractFacade<Utilisateur> {
    
    @PersistenceContext(unitName = "ricm5.ecom_Lubyb-ejb_ejb_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
       return em;
    }
    
    
    public UserFacade() {
        super(Utilisateur.class);
    }
    
    public boolean loginUsed(String login){
        return (em.find(Utilisateur.class, login) != null);
    }
    
    public void removeByLogin(String login){
        Utilisateur u = em.find(Utilisateur.class, login);
        if(u != null) {
            em.remove(u);
        }
    }

    public void add(Utilisateur v){
        try{
            if(em.find(Utilisateur.class, v.getLogin()) != null){
                em.merge(v);
            }else{
                em.persist(v);
            }
        }catch(EntityExistsException e){
            
        }
    }
    
    public String getRole(String login){
        return this.find(login).getRoleUser();
    }
}
