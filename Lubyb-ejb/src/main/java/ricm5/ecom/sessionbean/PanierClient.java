package ricm5.ecom.sessionbean;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import javax.ejb.LocalBean;
import javax.ejb.Stateful;
import ricm5.ecom.entitybean.Vin;

@Stateful
@LocalBean
public class PanierClient {

    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")
    
    private HashMap listeAchats;
    private String clientLogin;
    
    public PanierClient() {
        this.listeAchats = new HashMap();
    }

    public void setListeAchats(HashMap listeAchats) {
        this.listeAchats = listeAchats;
    }

    public void setClientLogin(String clientLogin) {
        this.clientLogin = clientLogin;
    }

    public HashMap getListeAchats() {
        return listeAchats;
    }

    public String getClientLogin() {
        return clientLogin;
    }
    

    
    // return 1 si ajout de 1 est OK
    // return 0 si ajout dépasse le stock
    // return -1 si rien n'a été ajouté
    public int addVin(Vin vin, int oldQuantite){
        int newQuantite;
        if(this.listeAchats.containsKey(vin)){
            // si vin est deja en panier, alors "merge" sa quantité
            if(vin.getStock() >= oldQuantite + 1){ // si stock suffisant pour ajouter 1 en plus
                this.listeAchats.put(vin, oldQuantite + 1);
                return 1;
            }else{ // stock insuffisant, ajouter le min(old_quantite, vin.getStock())
                newQuantite = Math.min(oldQuantite, vin.getStock());
                if(newQuantite > 0){
                    this.listeAchats.put(vin, newQuantite);
                    return 0;
                }else{
                    return -1;
                }
            }
        }else{
            // vin non-present dans le panier, ajouter simplement
            if(vin.getStock() >= oldQuantite+1){ // si stock suffisant
                this.listeAchats.put(vin, oldQuantite+1);
                return 1;
            }else{ // stock insuffisant
                newQuantite = Math.min(oldQuantite, vin.getStock());
                if(newQuantite > 0){
                    this.listeAchats.put(vin, newQuantite);
                    return 0;
                }else {
                    return -1;
                }
            }
        }
    }
    
    // decrementer de 1 la quantite de vin dans le panier
    public void decrementVin(Vin vin){
        if(this.listeAchats.containsKey(vin)){
            // si il reste plus de 1 bouteille, fait -1 sur sa quantité
            if((Integer)this.listeAchats.get(vin) > 1) {
                this.listeAchats.put(vin, (Integer)this.listeAchats.get(vin) - 1);
            }else{
                // quantite <= 1 : remove ce vin
                this.listeAchats.remove(vin);
            }
        }
    }
    
    public void removeVin(Vin vin){
        if(this.listeAchats.containsKey(vin)){
            this.listeAchats.remove(vin);
        }
    }
    
    public int total() {
        return this.listeAchats.size();
    }
    
    public double prixTotal() {
        double total = 0.0;
        Map.Entry entry;
        Iterator it = this.listeAchats.entrySet().iterator();
        while(it.hasNext()){
            entry = (Map.Entry) it.next();
            total = total + (((Vin) entry.getKey()).getPrix() * (Integer) entry.getValue() );
        }
        return total;
    }
    
    public boolean isVide(){
        return this.listeAchats.isEmpty();
    }
    
    public void vider(){
        this.listeAchats.clear();
    }
    
    public int getNbArticles(){
        Iterator it = this.listeAchats.entrySet().iterator();
        int nbArticles = 0;
        while(it.hasNext()){
            nbArticles += (Integer) ((Map.Entry)it.next()).getValue();
        }
        return nbArticles;
    }

}
