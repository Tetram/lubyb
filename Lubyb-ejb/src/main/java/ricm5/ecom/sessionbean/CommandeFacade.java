package ricm5.ecom.sessionbean;

import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.PriorityQueue;
import javax.ejb.Stateless;
import javax.persistence.EntityExistsException;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import ricm5.ecom.entitybean.Commande;
import ricm5.ecom.entitybean.Restaurant;
import ricm5.ecom.entitybean.Utilisateur;

@Stateless
public class CommandeFacade extends AbstractFacade<Commande> {
    
    @PersistenceContext(unitName = "ricm5.ecom_Lubyb-ejb_ejb_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
       return em;
    }

    public CommandeFacade() {
        super(Commande.class);
    }
    
    // créer et sauvegarde une commande dans la BD + return son ID
    public Commande createCommande(Date dateCommande, Utilisateur client, String nomClient, String prenomClient, HashMap produits, double montant, Restaurant resto, Date deliverDate){
        Commande c = new Commande();
       
        c.setDateCommande(dateCommande);
        c.setClient(client);
        c.setNomClient(nomClient);
        c.setPrenomClient(prenomClient);
        c.setProduits(produits); 
        c.setMontant(montant);
        c.setRestaurant(resto);
        c.setDroitBouchon(this.calculDroitBouchon(montant, resto));
        c.setDateToDeliver(deliverDate);
        c.setStatus(Commande.Status.PENDING);
        // inserer dans la BD
        this.add(c);
        
        return c;
    }

    public void add(Commande c) {
        try{
            em.persist(c);
        }catch(EntityExistsException e){            
        }
    }
    
    public Commande find(Long idCommande){
        return em.find(Commande.class, idCommande);
    }
    
    // verifier si la commande a été faite par ce client
    public boolean belongsTo(Commande c, String idClient){
        return c.getClient().getLogin().equals(idClient);
    }
    
    public Collection<Commande> findAllCommandes() {
      Query query = em.createQuery("SELECT e FROM Professor e");
      return (Collection<Commande>) query.getResultList();
    }
    
    public PriorityQueue<Commande> getMesCommandes (String idClient) {
        Iterator<Commande> it = this.findAllCommandes().iterator();
        Comparator dateComparator = new Comparator() {
            @Override
            public int compare(Object s1, Object s2){
                return ((Date)s1).compareTo((Date)s2);
            }
        };                
        PriorityQueue<Commande> resultat = new PriorityQueue<Commande>(0, dateComparator);
        Commande c;
        while (it.hasNext()) {
            c = it.next();
            if(c.getClient().getLogin().equals(idClient)) {
                resultat.add(c);
            }
        }
        return resultat ;
    }
    
    public double calculDroitBouchon(double montantCommande, Restaurant resto ){
        Restaurant r = em.find(Restaurant.class, resto.getSiret());
        if( r != null){
            return r.getTaux() * montantCommande;
        }else{
            return 0.0;
        }
    }
    
    public List<Commande> findByStatus(Commande.Status status){
        Query q = em.createQuery("select c from Commande c where c.status = :stat");
        q.setParameter("stat", status);
        return q.getResultList();
    }

}
