package ricm5.ecom.sessionbean;

import java.util.List;
import javax.ejb.Remote;
import ricm5.ecom.entitybean.Vin;
import ricm5.ecom.entitybean.Vin.Couleur;

@Remote
public interface VinRemoteInt {
    void add(Vin v);
    
    void delete(Long ref);
    void create(Vin entity);

    void edit(Vin entity);

    void remove(Vin entity);

    Vin find(Object id);

    List<Vin> findAll();

    List<Vin> findRange(int[] range);

    int count();
    
    List<Vin> search(String name, Couleur type, String price, String marge);
    
    List<Vin> findbyname(String name);
    
    List<Vin> findbytype(Couleur type, List<Vin> liste);
    
    List<Vin> findbyprice(double prix, List<Vin> liste, double marge);
    
    List<Vin> findbyregion(String region);
     
    List<Vin> findbynameandprice(String name, double price);
      
    List<Vin> findbynameandtype(String name, Couleur type);   
    
    List<Vin> findbypriceandtype(double prix, Couleur type);
    
    List<Vin> findbypriceandtypeandname(double prix, Couleur type, String name);

    Vin findbyid(Long id);

    void updateStockAfterCommande(ricm5.ecom.entitybean.Commande c);
    
}
