package ricm5.ecom.sessionbean;

import javax.ejb.Stateless;
import javax.persistence.EntityExistsException;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import ricm5.ecom.entitybean.Region;

@Stateless
public class RegionFacade extends AbstractFacade<Region> implements RegionRemote {
    @PersistenceContext(unitName = "ricm5.ecom_Lubyb-ejb_ejb_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public RegionFacade() {
        super(Region.class);
    }
    
    @Override
    public void delete(String ref){
        try{
            Region v = getEntityManager().find( Region.class, ref);
            getEntityManager().remove(v);
        }catch(Exception e){
            // Vin introuvable, faire ici l'action correspondant
        }
    }
    
    @Override
    public void add(Region v){
        try{
            if(em.find(Region.class, v.getId()) != null){
                em.merge(v);
            }else{
                em.persist(v);
            }
        }catch(EntityExistsException e){
            
        }
    }
    
}
