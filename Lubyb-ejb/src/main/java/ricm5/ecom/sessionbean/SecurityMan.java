
package ricm5.ecom.sessionbean;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import ricm5.ecom.entitybean.Utilisateur;

@Stateless
public class SecurityMan {
    
    private String initialisationCode = "lubyb";

    @PersistenceContext(unitName = "ricm5.ecom_Lubyb-ejb_ejb_1.0-SNAPSHOTPU")
    private EntityManager em;
    
    @EJB
    private PasswordEncryptionService pes;
    
    @EJB
    private UserFacade userFacade;
    
    public boolean isUserHasAccess (Object username, String requiredRole){
        if(username != null){
            Utilisateur user = em.find(Utilisateur.class, (String)username);
            if (user != null){
                return user.getRoleUser().equals(requiredRole);
            }
        }
        return false;
    }
    
    public void checkAccess( Object username, String requiredRole, String redirect, HttpServletResponse response) throws IOException{
        if(! this.isUserHasAccess(username, requiredRole)){
            response.sendRedirect("/Lubyb-web/pages/logon.jsp?accessDeniedMsg=\"Vous n'avez pas de droit d'accès à cette page.\"&redirect="+redirect);
        }
    }
    
    public void checkAccess( Object username, String requiredRole, String redirect,HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException{
        if(! this.isUserHasAccess(username, requiredRole)){
            request.setAttribute("from", "AdminPage");
            request.getRequestDispatcher("/Lubyb-web/pages/logon.jsp?accessDeniedMsg=\"Vous n'avez pas de droit d'accès à cette page.\"&redirect="+redirect).forward(request, response);
        }
    }
    
    public boolean createFirstAdmin (String initCode) throws NoSuchAlgorithmException, InvalidKeySpecException{
        byte[] encryptedPassword;
        byte[] salt ;
        
        if(this.initialisationCode.equals(initCode)){
            salt = pes.generateSalt();
            encryptedPassword = pes.getEncryptedPassword("admin", salt);

            Utilisateur user = new Utilisateur();
            user.setLogin("admin");
            user.setNom("admin");
            user.setPrenom("admin");
            user.setAdresseFact("NA");
            user.setEmail("NA");
            user.setTelephone("NA");
            user.setEncryptedPassword(encryptedPassword);
            user.setSalt(salt);
            user.setRoleUser(Utilisateur.Role.ADMIN) ;
            userFacade.add(user);
            return true;
        }
        return false;
    }

}
