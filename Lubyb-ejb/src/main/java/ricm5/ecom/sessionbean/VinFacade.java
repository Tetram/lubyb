package ricm5.ecom.sessionbean;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.ejb.Stateless;
import javax.persistence.EntityExistsException;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import ricm5.ecom.entitybean.Commande;
import ricm5.ecom.entitybean.Vin;
import ricm5.ecom.entitybean.Vin.Couleur;

@Stateless
public class VinFacade extends AbstractFacade<Vin> implements VinRemoteInt {

    @PersistenceContext(unitName = "ricm5.ecom_Lubyb-ejb_ejb_1.0-SNAPSHOTPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public VinFacade() {
        super(Vin.class);
    }

    @Override
    public List<Vin> search(String name, Couleur couleur, String prix, String marge) {
        List result = new ArrayList<Vin>();
        List resulttemp = new ArrayList<Vin>();
        double margePrix = 0.0;
        
        if (marge != null && !(marge.isEmpty())) {
            margePrix = Double.parseDouble(marge);
        }

        if (name == null || name.isEmpty()) {
            Query q = em.createQuery("select v from Vin v");
            List<Vin> liste = q.getResultList();

            if (couleur.equals(Couleur.UNKNOWN)) {
                if (prix == null || prix.isEmpty()) {
                    result = liste;
                } else {
                    result = findbyprice(Double.parseDouble(prix), liste, margePrix);
                }
            } else {
                resulttemp = findbytype(couleur, liste);
                if (prix == null || prix.isEmpty()) {
                    result = resulttemp;
                } else {
                    result = findbyprice(Double.parseDouble(prix), resulttemp, margePrix);
                    //                        if (result.size() == 0) {
//                            result = resulttemp;
//                        }
                }
            }
        } else {
            List<Vin> liste = this.findbyname(name);
            if (liste.size() > 0) {
                if (couleur.equals(Couleur.UNKNOWN)) {
                    if (prix.isEmpty()) {
                        result = liste;
                    } else {
                        result = findbyprice(Double.parseDouble(prix), liste, margePrix);
//                        if (result.size() == 0) {
//                            result = liste;
//                        }
                    }
                } else {
                    resulttemp = findbytype(couleur, liste);
                    if (prix.isEmpty()) {
                        result = resulttemp;
                    } else {
                        result = findbyprice(Double.parseDouble(prix), resulttemp, margePrix);
//                        if (result.size() == 0) {
//                            result = resulttemp;
//                        }
                    }
                }
            } else {
                if (couleur.compareTo(Couleur.UNKNOWN) == 0) {
                    if (prix.isEmpty()) {
                        result = liste;
                    } else {
                        Query q = em.createQuery("select v from Vin v");
                        liste = q.getResultList();
                        result = findbyprice(Double.parseDouble(prix), liste, margePrix);
                    }
                    if (result.size() == 0) {
                        result = liste;
                    }
                } else {
                    Query q = em.createQuery("select v from Vin v");
                    liste = q.getResultList();
                    resulttemp = findbytype(couleur, liste);
                    if (prix.isEmpty()) {
                        result = resulttemp;
                    } else {
                        result = findbyprice(Double.parseDouble(prix), resulttemp, margePrix);
//                        if (result.size() == 0) {
//                            result = resulttemp;
//                        }
                    }
                }
            }
        }
        return result;
    }

    @Override
    public List<Vin> findbyname(String name) {

        Query q = em.createQuery("select v from Vin v where v.nom = :name ");
        q.setParameter("name", name);

        return ((List<Vin>) q.getResultList());
    }

    @Override
    public List<Vin> findbytype(Couleur type, List<Vin> liste) {

        Iterator it = liste.iterator();
        List<Vin> result = new ArrayList<Vin>();
        Vin vin = new Vin();
        while (it.hasNext()) {
            vin = (Vin) it.next();
            if (vin.getType() == type) {
                result.add(vin);
            }
        }
        return result;
    }

    @Override
    public List<Vin> findbyprice(double prix, List<Vin> liste, double marge) {

        double prixmax = prix + marge;
        double prixmin = prix - marge;

        Iterator it = liste.iterator();
        List<Vin> result = new ArrayList<Vin>();
        Vin vin = new Vin();
        while (it.hasNext()) {
            vin = (Vin) it.next();
            if (vin.getPrix() <= prixmax && vin.getPrix() >= prixmin) {
                result.add(vin);
            }
        }
        return result;
    }

    @Override
    public List<Vin> findbyregion(String region) {
        Query q = em.createQuery("select v from Vin v where v.region = :region");
        q.setParameter("region", region);

        List<Vin> liste = (List<Vin>) q.getResultList();
        return liste;
    }

    @Override
    public Vin findbyid(Long id) {
        Query q = em.createQuery("select v from Vin v where v.id = :id");
        q.setParameter("id", id);

        return ((Vin) q.getSingleResult());
    }

    @Override
    public List<Vin> findbynameandprice(String name, double price) {
        Query q = em.createQuery("select v from Vin v where v.prix = :prix and v.nom like :name");
        q.setParameter("prix", price);
        q.setParameter("name", name);

        return ((List<Vin>) q.getResultList());
    }

    @Override
    public List<Vin> findbynameandtype(String name, Couleur type) {
        Query q = em.createQuery("select v from Vin v where v.type = :type and v.nom like :name");
        q.setParameter("type", type);
        q.setParameter("name", name);

        return ((List<Vin>) q.getResultList());
    }

    @Override
    public List<Vin> findbypriceandtype(double prix, Couleur type) {
        Query q = em.createQuery("select v from Vin v where v.prix = :prix and v.type = :type");
        q.setParameter("type", type);
        q.setParameter("prix", prix);

        List<Vin> liste = (List<Vin>) q.getResultList();

        return ((List<Vin>) q.getResultList());
    }

    @Override
    public List<Vin> findbypriceandtypeandname(double prix, Couleur type, String name) {
        Query q = em.createQuery("select v from Vin v where v.prix = :prix and v.type = :type and v.nom = :name");
        q.setParameter("type", type);
        q.setParameter("prix", prix);
        q.setParameter("name", name);

        return ((List<Vin>) q.getResultList());
    }

    @Override
    public void add(Vin v) {
        try {
            if (em.find(Vin.class, v.getId()) != null) {
                em.merge(v);
            } else {
                em.persist(v);
            }
        } catch (EntityExistsException e) {
        }
    }

    @Override
    public void delete(Long ref) {
        try {
            Vin v = getEntityManager().find(Vin.class, ref);
            getEntityManager().remove(v);
        } catch (Exception e) {
            // Vin introuvable, faire ici l'action correspondant
        }
    }
    public void decrementStockVin(Long idVin, int quantiteVendu){
        Vin v = this.findbyid(idVin);
        if(v != null){
            int oldStock = v.getStock();
            v.setStock(oldStock - quantiteVendu);
            em.merge(v);
        }
    }
    
    @Override
    public void updateStockAfterCommande(Commande c){
        Iterator it = c.getProduits().entrySet().iterator();
        Map.Entry entry;
        Vin v;
        int quantiteVendu;
        
        while(it.hasNext()){
            entry = (Map.Entry) it.next();
            v = (Vin) entry.getKey();
            quantiteVendu = (Integer) entry.getValue();
            this.decrementStockVin(v.getId(), quantiteVendu);
        }
    }
}
