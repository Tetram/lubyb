package ricm5.ecom.sessionbean;

import java.util.List;
import javax.ejb.Remote;
import ricm5.ecom.entitybean.Region;

@Remote
public interface RegionRemote {
    void add(Region v);
    
    void delete(String ref);
    void create(Region entity);

    void edit(Region entity);

    void remove(Region entity);

    Region find(Object id);

    List<Region> findAll();

    List<Region> findRange(int[] range);

    int count();
}
