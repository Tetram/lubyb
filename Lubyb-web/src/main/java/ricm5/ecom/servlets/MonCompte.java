package ricm5.ecom.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import ricm5.ecom.entitybean.Restaurant;
import ricm5.ecom.entitybean.Utilisateur;
import ricm5.ecom.sessionbean.PanierClient;
import ricm5.ecom.sessionbean.RestaurantRemote;
import ricm5.ecom.sessionbean.SecurityMan;
import ricm5.ecom.sessionbean.UserFacade;

/**
 *
 * @author Aurelien
 */
@WebServlet(name = "MonCompte", urlPatterns = {"/MonCompte"})
public class MonCompte extends HttpServlet {
    @EJB
    private RestaurantRemote restauFacade;
    
    @EJB
    private UserFacade userFacade;
    
    @EJB
    private SecurityMan sm;
    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        Utilisateur me = null;
        
        HttpSession session = request.getSession(); 
        
        if(session.getAttribute("panier")==null){
            PanierClient panier_client = new PanierClient(); 
            session.setAttribute("panier", panier_client);
        }
        //Securité : verifier si loggé...
        
        //Recupérer les informations sur le client connecté !! (session ??)
        String monLogin = (String) session.getAttribute("login");
        if(isOk(monLogin)){
            me = userFacade.find(monLogin);
        }
        
        String siret = request.getParameter("siret");
        String nom = request.getParameter("nom");
        String adresse = request.getParameter("adresse");
        
        try {
            if(isOk(siret) && isOk(nom) && isOk(adresse) && me != null){
                Restaurant restauToAdd = new Restaurant();
                restauToAdd.setSiret(siret);
                restauToAdd.setName(nom);
                restauToAdd.setAdresse(adresse);
                restauToAdd.setSolde(0);
                restauToAdd.setTaux(0.05);
                restauToAdd.setStatus(Restaurant.Status.PENDING);
                restauToAdd.setResponsable(me);
                
                restauFacade.add(restauToAdd);
            }
            if(me != null){
                request.setAttribute("me", me);
                request.setAttribute("mesRestaus", restauFacade.findByUserInCharge(me));
                request.getRequestDispatcher("pages/moncompte.jsp").forward(request, response);
            }
            else{
                request.getRequestDispatcher("pages/erreur.jsp").forward(request, response);
            }
        } finally {            
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private boolean isOk(String s){
        if(s == null){
            return false;
        }
        return (!s.isEmpty());
    }
}

