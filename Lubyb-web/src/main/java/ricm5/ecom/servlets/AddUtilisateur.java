/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ricm5.ecom.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import ricm5.ecom.entitybean.Utilisateur;
import ricm5.ecom.sessionbean.PanierClient;
import ricm5.ecom.sessionbean.PasswordEncryptionService;
import ricm5.ecom.sessionbean.SecurityMan;
import ricm5.ecom.sessionbean.UserFacade;

@WebServlet(name = "AddUtilisateur", urlPatterns = {"/AddUtilisateur"})
public class AddUtilisateur extends HttpServlet {

    @EJB
    private UserFacade userFacade;
    
    @EJB
    private PasswordEncryptionService pes;
    
    @EJB
    private SecurityMan sm;
    

    
    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
        HttpSession session = request.getSession(); 
        
        if(session.getAttribute("panier")==null){
            PanierClient panier_client = new PanierClient(); 
            session.setAttribute("panier", panier_client);
        }
        
        // Verify user's access right : 
        // if authenticated user is not allowed to access to this ressource, redirect to Logon page
        // else continue request treatement
        sm.checkAccess(session.getAttribute("login"), "ADMIN", this.getServletName(), response);

        String nom=request.getParameter("nom");
        String prenom=request.getParameter("prenom");
        String username=request.getParameter("username");
        String password=request.getParameter("password");
        String roleUser=request.getParameter("roleUser");
        String adresse_fact=request.getParameter("adresse_fact") ;
        String adresse_liv=request.getParameter("adresse_liv") ;
        String telephone=request.getParameter("telephone") ;
        byte[] encryptedPassword = null;
        byte[] salt = null;

       if ((username!=null) && (password != null)) {
           // create a new User :
           Utilisateur user = new Utilisateur();
           try {
               salt = pes.generateSalt();
               encryptedPassword = pes.getEncryptedPassword(password, salt); 
            } catch (NoSuchAlgorithmException ex) {
                Logger.getLogger(AddUtilisateur.class.getName()).log(Level.SEVERE, null, ex);
            } catch (InvalidKeySpecException ex) {
                Logger.getLogger(AddUtilisateur.class.getName()).log(Level.SEVERE, null, ex);
            }
           user.setLogin(username);
           user.setEncryptedPassword(encryptedPassword);
           user.setSalt(salt);
           if(nom != null) {user.setNom(nom); }
           if(prenom != null) {user.setPrenom(prenom); }
           // assign role to user 
           if(roleUser != null) {
               if(roleUser.equals("Admin")){
                   user.setRoleUser(Utilisateur.Role.ADMIN) ;
               }else{
                   if(roleUser.equals("Client")){
                       user.setRoleUser(Utilisateur.Role.CLIENT) ;
                       if(adresse_fact != null) {user.setAdresseFact(adresse_fact); }
                       if(adresse_liv != null) {user.setAdresseLiv(adresse_liv); }
                       if(telephone != null) {user.setTelephone(telephone); }
                   }else{
                       // here role must be Restaurateur
                       user.setRoleUser(Utilisateur.Role.RESTAURATEUR) ;
                   }
               }
           }
           // Add this user into UserFacade:
           userFacade.add(user);
           response.sendRedirect("ListUtilisateur");
        }
        PrintWriter out = response.getWriter();
        try {
            /* TODO output your page here. You may use following sample code. */
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet AddUtilisateur</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet AddUtilisateur at " + request.getContextPath() + "</h1>");

            out.println("<form method=\"POST\">");
            out.println("Nom: <input type='text' name='nom'><br/>");
            out.println("Prénom: <input type='text' name='prenom'><br/>");
            out.println("Login: <input type='text' name='username'><br/>");
            out.println("Mot de passe: <input type='password' name='password'><br/>");
            out.println("Role: "+
                    "<select name=\"roleUser\">"+
                        "<option value=\"Client\">Client</option>"+
                        "<option value=\"Admin\">Admin</option>"+
                        "<option value=\"Restaurateur\">Restaurateur</option>"+
                     "</select><br/>");
            out.println("<input type='submit'><br/>");
            out.println("</form>");            
            out.println("<a href='ListUtilisateur'>Users list</a>");
            out.println("</br><a href='/Lubyb-web'>Home Page</a>");
            out.println("</body>");
            out.println("</html>");
        } finally {            
            out.close();
        }
}

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
