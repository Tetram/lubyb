package ricm5.ecom.servlets;

import java.io.IOException;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import ricm5.ecom.entitybean.Restaurant;
import ricm5.ecom.sessionbean.PanierClient;
import ricm5.ecom.sessionbean.RestaurantRemote;
import ricm5.ecom.sessionbean.SecurityMan;

@WebServlet(name = "AcceptRestau", urlPatterns = {"/AdminPage/AcceptRestau"})
public class AcceptRestau extends HttpServlet {
    
    @EJB
    private RestaurantRemote restauFacade;
    
    @EJB
    private SecurityMan sm;

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        HttpSession session = request.getSession(); 
        
        if(session.getAttribute("panier")==null){
            PanierClient panier_client = new PanierClient(); 
            session.setAttribute("panier", panier_client);
        }
        
        if(session.getAttribute("panier")==null){
            PanierClient panier_client = new PanierClient(); 
            session.setAttribute("panier", panier_client);
        }
        sm.checkAccess(session.getAttribute("login"), "ADMIN", this.getServletName(), response);
        
        String siret = request.getParameter("siret");
        Restaurant toValidate = restauFacade.find(siret);
        toValidate.setStatus(Restaurant.Status.VALIDATED);
        restauFacade.edit(toValidate);
        
        String pathToGo = "/Lubyb-web/AdminPage/ValidationPartenariat"+"?siret="+siret+"&done=accepte";
        response.sendRedirect(pathToGo);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
