/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ricm5.ecom.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.jms.JMSException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import ricm5.ecom.entitybean.Vin;
import ricm5.ecom.sessionbean.PanierClient;
import ricm5.ecom.sessionbean.SecurityMan;
import ricm5.ecom.sessionbean.VinRemoteInt;

/**
 *
 * @author Quan
 */
@WebServlet(name = "DeleteVin", urlPatterns = {"/AdminPage/DeleteVin"})
public class DeleteVin extends HttpServlet {
    
    @EJB
    private VinRemoteInt vinFacade;
    
    @EJB
    private SecurityMan sm;
    
    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, JMSException {
        
        // Verify user's access right : 
        // if authenticated user is not allowed to access to this ressource, redirect to Logon page
        // else continue request treatement
        HttpSession session = request.getSession(); 
        
        if(session.getAttribute("panier")==null){
            PanierClient panier_client = new PanierClient(); 
            session.setAttribute("panier", panier_client);
        }
        sm.checkAccess(session.getAttribute("login"), "ADMIN", this.getServletName(), response);
        
        List vins = vinFacade.findAll();
        session.setAttribute("liste", vins);
        
        String ref=request.getParameter("ref");
        if(ref != null){  
            if(ref.equals("add")){response.sendRedirect("AddVin");}
            else {
            vinFacade.delete(Long.parseLong(ref));
            vins = vinFacade.findAll();
            session.setAttribute("liste", vins);
            response.sendRedirect("DeleteVin");}
        }
        else {
            request.getRequestDispatcher("../pages/deletevin.jsp").forward(request, response);
        }
        
        
    
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (JMSException ex) {
            Logger.getLogger(DeleteVin.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (JMSException ex) {
            Logger.getLogger(DeleteVin.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
