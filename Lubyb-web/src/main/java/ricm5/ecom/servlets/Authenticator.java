/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ricm5.ecom.servlets;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import ricm5.ecom.entitybean.Utilisateur;
import ricm5.ecom.sessionbean.PanierClient;
import ricm5.ecom.sessionbean.PasswordEncryptionService;
import ricm5.ecom.sessionbean.UserFacade;

/**
 *
 * @author Quan
 */
@WebServlet(name = "Authenticator", urlPatterns = {"/Authenticator"})
public class Authenticator extends HttpServlet {

    @EJB
    private UserFacade userFacade;
    
    @EJB
    private PasswordEncryptionService pes;
    
    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, NoSuchAlgorithmException, InvalidKeySpecException {
        response.setContentType("text/html;charset=UTF-8");
        
        String login=request.getParameter("login");
        String password=request.getParameter("password");
        String redirect=request.getParameter("redirect");
        
        HttpSession session = request.getSession(); 
        
        if(session.getAttribute("panier")==null){
            PanierClient panier_client = new PanierClient(); 
            session.setAttribute("panier", panier_client);
        }
        
        if(login == null || password == null){
            response.sendRedirect("/Lubyb-web/pages/logon.jsp?auth-error=true&redirect="+redirect);
            //request.getRequestDispatcher("/Lubyb-web/pages/logon.jsp?auth-error=true&redirect="+redirect).forward(request, response);
        }else{
            Utilisateur u = userFacade.find(login); // look for the user by login
            if(u == null){ // username not found
                response.sendRedirect("/Lubyb-web/pages/logon.jsp?auth-error=true&redirect="+redirect);
                //request.getRequestDispatcher("/Lubyb-web/pages/logon.jsp?auth-error=true&redirect="+redirect).forward(request, response);
            }else{
                // if password matches:
                if(pes.authenticate(password, u.getEncryptedPassword(), u.getSalt())){
                    // create a new session
                    session = request.getSession(true);
                    session.setMaxInactiveInterval(3000);
                    session.setAttribute("login", login);   
                    session.setAttribute("userRole", userFacade.find(login).getRoleUser());
                    
                    PanierClient sonPanier = (PanierClient) session.getAttribute("panier");
                    if(sonPanier != null){
                        sonPanier.setClientLogin(login);
                        session.setAttribute("panier", sonPanier);
                    }
                    
                    if(redirect != null){
                        response.sendRedirect(redirect);
                        //request.getRequestDispatcher(redirect).forward(request, response);
                    }else{
                        response.sendRedirect("/Lubyb-web");
                        //request.getRequestDispatcher("/Lubyb-web").forward(request, response);
                    }
                    
                }else{
                    response.sendRedirect("/Lubyb-web/pages/logon.jsp?auth-error=true&redirect="+redirect);
                    //request.getRequestDispatcher("/Lubyb-web/pages/logon.jsp?auth-error=true&redirect="+redirect).forward(request, response);
                }
            }
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(Authenticator.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InvalidKeySpecException ex) {
            Logger.getLogger(Authenticator.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(Authenticator.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InvalidKeySpecException ex) {
            Logger.getLogger(Authenticator.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
