package ricm5.ecom.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import ricm5.ecom.entitybean.Vin;
import ricm5.ecom.sessionbean.PanierClient;
import ricm5.ecom.sessionbean.VinRemoteInt;

@WebServlet(name = "Panier", urlPatterns = {"/Panier"})
public class Panier extends HttpServlet {
    
    @EJB
    private VinRemoteInt vinFacade;

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
       
        HttpSession session = request.getSession(); 
        
        if(session.getAttribute("panier")==null){
            PanierClient panier_client = new PanierClient(); 
            session.setAttribute("panier", panier_client);
        }
        PanierClient panier = (PanierClient) session.getAttribute("panier");
        
        try {
            
            /**************************/
            /* ajout du vin au panier */
            /**************************/
            String id = request.getParameter("id") ;
            String mode = request.getParameter("mode") ;
            int ajoutOK = -100;
            int old_quantite = 0;
            Vin v;
            if(id!= null && mode!=null){
                v = vinFacade.find(Long.parseLong(id));
                
                if(panier.getListeAchats().containsKey(v)){
                    old_quantite = (Integer) panier.getListeAchats().get(v);
                }
                
                if(mode.equals("addtocard")) {
                    ajoutOK = panier.addVin(v, old_quantite);
                }
                else {
                    if(mode.equals("decrement")) {
                        panier.decrementVin(v);
                    }else{
                         if(mode.equals("increment")) {
                             ajoutOK = panier.addVin(v, old_quantite);
                         }else{
                             if(mode.equals("remove")) {panier.removeVin(v);}
                         }                      
                    }
                }
            }
            
            // remettre le panier dans la session
            session.setAttribute("panier", panier);
            request.setAttribute("id", null);
            request.setAttribute("mode", null);
            request.setAttribute("ajoutOK", ajoutOK);
            
            request.getRequestDispatcher("/pages/panier.jsp").forward(request, response);
            
        } finally {            
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
