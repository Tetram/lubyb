package ricm5.ecom.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import ricm5.ecom.entitybean.Utilisateur;
import ricm5.ecom.sessionbean.PanierClient;
import ricm5.ecom.sessionbean.PasswordEncryptionService;
import ricm5.ecom.sessionbean.UserFacade;

/**
 *
 * @author Aurelien
 */
@WebServlet(name = "Inscription", urlPatterns = {"/Inscription"})
public class Inscription extends HttpServlet {

    @EJB
    private UserFacade userFacade;
    @EJB
    private PasswordEncryptionService pes;

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String nom = request.getParameter("nom");
        String prenom = request.getParameter("prenom");
        String username = request.getParameter("login");
        String password = request.getParameter("password");
        String adresse_fact = request.getParameter("adresse_fact");
        String adresse_liv = request.getParameter("adresse_liv");
        String telephone = request.getParameter("telephone");
        String mail = request.getParameter("mail");
        byte[] encryptedPassword = null;
        byte[] salt = null;

        HttpSession session = request.getSession(); 
        
        if(session.getAttribute("panier")==null){
            PanierClient panier_client = new PanierClient(); 
            session.setAttribute("panier", panier_client);
        }
        if ((username != null) && (password != null)) {
            if (userFacade.find(username) != null) {         
                request.setAttribute("loginused", username);
                request.getRequestDispatcher("pages/inscription.jsp").forward(request, response);
                
            } else {
                // create a new User :
                Utilisateur user = new Utilisateur();
                try {
                    salt = pes.generateSalt();
                    encryptedPassword = pes.getEncryptedPassword(password, salt);
                } catch (NoSuchAlgorithmException ex) {
                    Logger.getLogger(Inscription.class.getName()).log(Level.SEVERE, null, ex);
                } catch (InvalidKeySpecException ex) {
                    Logger.getLogger(Inscription.class.getName()).log(Level.SEVERE, null, ex);
                }
                user.setLogin(username);
                user.setEncryptedPassword(encryptedPassword);
                user.setSalt(salt);
                if (nom != null) {
                    user.setNom(nom);
                }
                if (prenom != null) {
                    user.setPrenom(prenom);
                }
                // assign role to user 

                user.setRoleUser(Utilisateur.Role.CLIENT);
                if (adresse_fact != null) {
                    user.setAdresseFact(adresse_fact);
                }
                if (adresse_liv != null) {
                    user.setAdresseLiv(adresse_liv);
                }
                if (telephone != null) {
                    user.setTelephone(telephone);
                }
                if (mail != null) {
                    user.setEmail(mail);
                }
                userFacade.add(user);
                response.sendRedirect("/Lubyb-web/pages/inscriptiondone.jsp");
            }
        } else {
            request.getRequestDispatcher("pages/inscription.jsp").forward(request, response);
        }

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
