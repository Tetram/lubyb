package ricm5.ecom.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import ricm5.ecom.entitybean.Region;
import ricm5.ecom.entitybean.Vin;
import ricm5.ecom.sessionbean.PanierClient;
import ricm5.ecom.sessionbean.RegionRemote;
import ricm5.ecom.sessionbean.SecurityMan;
import ricm5.ecom.sessionbean.VinRemoteInt;

@WebServlet(name = "AddVin", urlPatterns = {"/AdminPage/AddVin"})
public class AddVin extends HttpServlet {

//    @Resource(mappedName="jms/VinMessageFactory")
//    private  ConnectionFactory connectionFactory;
//
//    @Resource(mappedName="jms/VinMessage")
//    private Queue queue;
    @EJB
    private VinRemoteInt vinFacade;
    @EJB
    private RegionRemote regionFacade;
    @EJB
    private SecurityMan sm;
    
    protected Vin.Couleur StringToCouleur(String couleur) {
        Vin.Couleur c = Vin.Couleur.UNKNOWN;
        String rouge = "ROUGE";
        String blanc = "BLANC";
        String jaune = "JAUNE";
        String rose = "ROSE";
        if (couleur != null) {
            if (couleur.equals(rouge)) {
                c = Vin.Couleur.ROUGE;
            } else if (couleur.equals(blanc)) {
                c = Vin.Couleur.BLANC;
            } else if (couleur.equals(jaune)) {
                c = Vin.Couleur.JAUNE;
            } else if (couleur.equals(rose)) {
                c = Vin.Couleur.ROSE;
            }
        }
        return c;
    }

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        String ref = request.getParameter("reference");
        String nom = request.getParameter("nom");
        String prix = request.getParameter("prix");
        String annee = request.getParameter("annee");
        String stock = request.getParameter("stock");
        String region = request.getParameter("region");
        String couleur = request.getParameter("couleur");
        String imageUri = request.getParameter("url");
        
        
        HttpSession session = request.getSession(); 
        
        if(session.getAttribute("panier")==null){
            PanierClient panier_client = new PanierClient(); 
            session.setAttribute("panier", panier_client);
        }
        // Verify user's access right : 
        // if authenticated user is not allowed to access to this ressource, redirect to Logon page
        // else continue request treatement
        sm.checkAccess(session.getAttribute("login"), "ADMIN", this.getServletName(), response);
        
        

        if ((ref != null)) {

            // here we create NewsEntity, that will be add into vinFacade
            Vin e = new Vin();
            e.setId(Long.parseLong(ref));
            if (nom != null) {
                e.setNom(nom);
            }
            if (prix != null) {
                e.setPrix(Double.parseDouble(prix));
            }
            if (annee != null) {
                e.setAnnee(Integer.parseInt(annee));
            }
            if (stock != null) {
                e.setStock(Integer.parseInt(stock));
            }
            if(region != null)  {
             Region regionTemp = regionFacade.find(region);
                if(regionTemp == null){
                    regionTemp = new Region();
                    regionTemp.setId(region);
                    regionFacade.add(regionTemp);
                }
                e.setRegion(regionTemp);
            }
            if (couleur != null) {
                e.setType(StringToCouleur(couleur));
            }
            if (imageUri != null) {
                e.setImageUri(imageUri);
            } else {
                e.setImageUri("http://www.liste-vin.fr/wp-content/uploads/2009/06/chateau-fonrazade-grand-cru-300x300.jpg");
            }

            // Add vin e into vinFacade
            vinFacade.add(e);
            response.sendRedirect("DeleteVin");

        }
        
        else {request.getRequestDispatcher("../pages/addvin.jsp").forward(request, response);}

        /*PrintWriter out = response.getWriter();
        try {
            
            out.println("<link href=\"/Lubyb-web/bootstrap/css/bootstrap.css\" rel=\"stylesheet\">\n");
            out.println("<link href=\"/Lubyb-web/css/page.css\" rel=\"stylesheet\">");
            
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet AddVin</title>");
            out.println("</head>");
            out.println("<body id='body'>");
            
            out.println("<div id=page>");
            
            out.println("<jsp:include page='Lubyb-web/elements/header.jsp'>");
            out.println("<jsp:param name='pagecourante' value='Ajout'/>");
            out.println("</jsp:include>");
            
            out.println("<h1>Ajouter un vin au catalogue</h1>");
            out.println("<form>");
            out.println("Reference (code barre): <input type='text' name='ref'><br/>");
            out.println("Nom: <input type='text' name='nom'><br/>");
            out.println("Région: <input type='text' name='region'><br/>");
            out.println("Annee: <input type='text' name='annee'><br/>");
            out.println("Couleur: "
                    + "<select name=\"couleur\">"
                    + "<option value=\"Rouge\">Rouge</option>"
                    + "<option value=\"Rose\">Rosé</option>"
                    + "<option value=\"Blanc\">Blanc</option>"
                    + "<option value=\"Jaune\">Jaune</option>"
                    + "</select><br/>");
            out.println("Prix: <input type='text' name='prix'><br/>");
            out.println("Stock: <input type='text' name='stock'><br/>");
            out.println("URL image: <input type='text' name='imageUri'><br/>");
            out.println("<input class=\"btn btn-primary\" type='submit'><br/>");
            out.println("</form>");
            out.println("<a href='/Lubyb-web/Vins'>Back</a>");
            out.println("<jsp:include page='../elements/footer.jsp'/>");
            out.println("<div id='basdepage'></div>");
            out.println("</div>");
            out.println("</body>");
            out.println("</html>");
        } finally {
            out.close();
        }*/
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
