package ricm5.ecom.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import ricm5.ecom.entitybean.Vin;
import ricm5.ecom.entitybean.Vin.Couleur;
import ricm5.ecom.sessionbean.PanierClient;
import ricm5.ecom.sessionbean.VinRemoteInt;

@WebServlet(name = "SearchVin", urlPatterns = {"/Vins"})
public class SearchVin extends HttpServlet {

    @EJB
    private VinRemoteInt vinFacade;

    protected Couleur StringToCouleur(String couleur) {
        Couleur c = Couleur.UNKNOWN;
        String rouge = "ROUGE";
        String blanc = "BLANC";
        String jaune = "JAUNE";
        String rose = "ROSE";
        if (couleur != null) {
            if (couleur.equals(rouge)) {
                c = Couleur.ROUGE;
            } else if (couleur.equals(blanc)) {
                c = Couleur.BLANC;
            } else if (couleur.equals(jaune)) {
                c = Couleur.JAUNE;
            } else if (couleur.equals(rose)) {
                c = Couleur.ROSE;
            }
        }
        return c;
    }

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        PrintWriter out = response.getWriter();
        String notif = new String();
        response.setContentType("text/html;charset=UTF-8");
        HttpSession session = request.getSession(); 
        
        if(session.getAttribute("panier")==null){
            PanierClient panier_client = new PanierClient(); 
            session.setAttribute("panier", panier_client);
        }
        String ref = request.getParameter("ref");
        String type = request.getParameter("type");
        String nom = request.getParameter("nom");
        String prix = request.getParameter("prix");
        String marge = request.getParameter("marge");

//        String id = (String) request.getParameter("id");
//        String redirect = (String) request.getParameter("redirect");
        String id = (String) request.getParameter("id");
        String redirect = (String) request.getParameter("redirect");
        if (id == null && redirect == null) {
            System.out.println("null null");
        } else {
            System.out.println("SearchVin : ID = " + id + "Redirect = " + redirect);
        }



        try {
            List<Vin> liste;
            if (ref != null && !ref.isEmpty()) {
                Vin v = vinFacade.find(Long.parseLong(ref));
                liste = new ArrayList<Vin>();
                if (v != null) {
                    liste.add(v);
                }
                request.setAttribute("message", notif);
                request.setAttribute("liste", liste);
                request.getRequestDispatcher("pages/vins.jsp").forward(request, response);
            } else {
                Couleur c = this.StringToCouleur(type);

                if (marge != null && !marge.equals("")) {
                    liste = vinFacade.search(nom, c, prix, marge);
                } else {
                    liste = vinFacade.search(nom, c, prix, "2");
                }
                request.setAttribute("message", notif);
                request.setAttribute("liste", liste);
                request.getRequestDispatcher("pages/vins.jsp").forward(request, response);
            }


        } catch (NullPointerException ex) {
            ex.printStackTrace();
        }


        try {
            /* TODO output your page here. You may use following sample code. */
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet SearchVin</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet SearchVin at " + request.getContextPath() + "</h1>");

            out.println("<form>");
            out.println("Reference (code barre): <input type='text' name='ref'><br/>");
            out.println("Nom: <input type='text' name='nom'><br/>");
            out.println("Prix: <input type='text' name='prix'><br/>");
            out.println("Marge: <input type='text' name='marge'><br/>");
            out.println("Type: <select name='type'> <option></option> "
                    + "<option value=ROUGE>Rouge</option>"
                    + "<option value=BLANC>Blanc</option>"
                    + "<option value=ROSE>Rose</option>"
                    + "<option value=JAUNE>Jaune</option>"
                    + "</select><br/>");
            out.println("<input type='submit'><br/>");
            out.println("</form>");

            out.println("</body>");
            out.println("</html>");
        } finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
