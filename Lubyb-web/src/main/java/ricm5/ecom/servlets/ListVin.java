package ricm5.ecom.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import ricm5.ecom.entitybean.Vin;
import ricm5.ecom.sessionbean.PanierClient;
import ricm5.ecom.sessionbean.SecurityMan;
import ricm5.ecom.sessionbean.VinRemoteInt;


@WebServlet(name = "ListVin", urlPatterns = {"/AdminPage/ListVin"})
public class ListVin extends HttpServlet {
    @EJB
    private VinRemoteInt vinFacade;
    
    @EJB
    private SecurityMan sm;    

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        HttpSession session = request.getSession(); 
        
        if(session.getAttribute("panier")==null){
            PanierClient panier_client = new PanierClient(); 
            session.setAttribute("panier", panier_client);
        }
        
                // Verify user's access right : 
        // if authenticated user is not allowed to access to this ressource, redirect to Logon page
        // else continue request treatement
        sm.checkAccess(session.getAttribute("login"), "ADMIN", this.getServletName(), response);    
        
        PrintWriter out = response.getWriter();      
        
        try {
            /* TODO output your page here. You may use following sample code. */
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ListVin</title>"); 
            //out.println("<script type=\"text/javascript\" src=\"http://ajax.googleapis.com/ajax/libs/jquery/1.4/jquery.min.js\"></script>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ListVin at " + request.getContextPath() + "</h1>");
              
            ///// Affichage de la liste de vins
            out.println("<table>");
            out.println("<tr>");
            out.println("<td><b>Nom</b></td>");
            out.println("<td><b>Reference</b></td>");
            out.println("<td><b>Annee</b></td>");
            out.println("<td><b>Prix</b></td>");
            out.println("<td><b>Region</b></td>");
            out.println("<td><b>Type</b></td>");
            out.println("<td><b>Stock</b></td>");
            out.println("</tr>");
            List vins = vinFacade.findAll();
            for (Iterator it = vins.iterator(); it.hasNext();) {
              out.println("<tr>");
              Vin elem = (Vin) it.next();
              out.println(" <td><b>"+elem.getNom()+" </b><br /></td>");
              out.println("<td>"+elem.getId()+"<br /></td> ");
              out.println("<td>"+elem.getAnnee()+"<br /></td> ");
              out.println("<td>"+elem.getPrix()+"<br /></td> ");
              if(elem.getRegion()!=null){
                  out.println("<td>"+elem.getRegion().getId()+"<br /></td> ");
              }
              else{
                  out.println("<td>"+"null"+"<br /></td> ");
              }
              out.println("<td>"+elem.getType()+"<br /></td> ");
              out.println("<td>"+elem.getStock()+"<br /></td> ");
              out.println("</tr>");
              
            }
            
            out.println("</table>");
            ///// FIN DE LALISTE DE VIN /////////
            
            out.println("<a href='AddVin'>Add new wine</a>");
            out.println("<a href='DeleteVin'>Remove a wine</a>");
            out.println("<br />");
            out.println("</body>");
            out.println("</html>");
        } finally {            
            out.close();
        }
                       
    }
    
    

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
