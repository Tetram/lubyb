package ricm5.ecom.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import ricm5.ecom.entitybean.Utilisateur;
import ricm5.ecom.sessionbean.SecurityMan;
import ricm5.ecom.sessionbean.UserFacade;



@WebServlet(name = "ListUtilisateur", urlPatterns = {"/ListUtilisateur"})
public class ListUtilisateur extends HttpServlet {
    @EJB
    private UserFacade userFacade;

    @EJB
    private SecurityMan sm;
     
    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
        // Verify user's access right : 
        // if authenticated user is not allowed to access to this ressource, redirect to Logon page
        // else continue request treatement
        HttpSession session = request.getSession();
        sm.checkAccess(session.getAttribute("login"), "ADMIN", this.getServletName(), response);        
        
        PrintWriter out = response.getWriter();
        try {
            /* TODO output your page here. You may use following sample code. */
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ListUtilisateur</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ListUtilisateur at " + request.getContextPath() + "</h1>");
            
            
            ///// Print users list
            out.println("<table>");
            out.println("<tr>");
            out.println("<td><b>Nom</b></td>");
            out.println("<td><b>Prénom</b></td>");
            out.println("<td><b>Login</b></td>");
            out.println("<td><b>Mot de passe</b></td>");
            out.println("<td><b>Salt</b></td>");
            out.println("<td><b>Role</b></td>");
            out.println("</tr>");
            List users = userFacade.findAll();
            for (Iterator it = users.iterator(); it.hasNext();) {
              out.println("<tr>");
              Utilisateur elem = (Utilisateur) it.next();
              out.println(" <td><b>"+elem.getNom()+" </b><br /></td>");
              out.println("<td>"+elem.getPrenom()+"<br /></td> ");
              out.println("<td>"+elem.getLogin()+"<br /></td> ");
              out.println("<td>"+ elem.getEncryptedPassword().toString()+"<br /></td> ");
              out.println("<td>"+ elem.getSalt().toString()+"<br /></td> ");
              out.println("<td>"+elem.getRoleUser()+"<br /></td> ");
              out.println("</tr>");
            
            }
            out.println("</table>");
            ///// End of user list /////////
            
            out.println("<a href='AddUtilisateur'>Add new user</a>");
            out.println("<a href='DeleteUtilisateur'>Remove a user</a>");
            out.println("</br><a href='/Lubyb-web'>Home Page</a>");
            out.println("</body>");
            out.println("</html>");
        } finally {            
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
