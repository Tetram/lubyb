package ricm5.ecom.servlets;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import ricm5.ecom.entitybean.Commande;
import ricm5.ecom.entitybean.Restaurant;
import ricm5.ecom.entitybean.Utilisateur;
import ricm5.ecom.sessionbean.CommandeFacade;
import ricm5.ecom.sessionbean.PanierClient;
import ricm5.ecom.sessionbean.RestaurantRemote;
import ricm5.ecom.sessionbean.UserFacade;
import ricm5.ecom.sessionbean.VinRemoteInt;

@WebServlet(name = "ValiderCommande", urlPatterns = {"/ValiderCommande"})
public class ValiderCommande extends HttpServlet {

    @EJB
    private RestaurantRemote rf;
    @EJB
    private CommandeFacade cf;
    @EJB
    private VinRemoteInt vf;
    @EJB
    private UserFacade uf;

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        HttpSession session = request.getSession(); 
        
        if(session.getAttribute("panier")==null){
            PanierClient panier_client = new PanierClient(); 
            session.setAttribute("panier", panier_client);
        }

        // get client panier
        PanierClient panier = (PanierClient) session.getAttribute("panier");

        // get client identifiant
        String loginUser = (String) session.getAttribute("login");

        // get id_restaurant pour livraison + adresse facturation
        String id_resto = request.getParameter("restaurant");
        String addr_fact = request.getParameter("addr_fact");
        String nomClient = request.getParameter("nomClient");
        String prenomClient = request.getParameter("prenomClient");
        String deliveringDate = request.getParameter("dateLivraison");
        String deliveringTime = request.getParameter("heure");
        String dateToDeliver = deliveringDate + "-" + deliveringTime;

        if (loginUser == null) { // utilisateur n'est pas identifié, alors lui demander son identification
            response.sendRedirect("/Lubyb-web/pages/logon.jsp?redirect=" + this.getServletName());
        } else {
            if (panier.isVide()) {
                request.getRequestDispatcher("Panier").forward(request, response);
            } else {
                if (id_resto == null || addr_fact == null) {
                    // requete incomplete pour créer une commande => rediriger vers infoCommande.jsp saisir des info
                    request.setAttribute("restaurants", rf.findByStatus(Restaurant.Status.VALIDATED));
                    request.getRequestDispatcher("pages/commander.jsp").forward(request, response);
                } else {
                    // créer et sauvegarder une commande dans la BD
                    Restaurant resto = rf.find(id_resto);
                    Utilisateur client = uf.find(loginUser);
                    // for DEBUG
                    if (resto == null) {
                    }
                    if (client == null) {
                    }
                    try {
                        ///////////
                        Date date = new SimpleDateFormat("yyyy-MM-dd-HH:mm", Locale.FRANCE).parse(dateToDeliver);
                        Commande c = cf.createCommande(new Date(), client, nomClient, prenomClient, panier.getListeAchats(), panier.prixTotal(), resto, date);

                        // Mettre à jour le stock de vin
                        vf.updateStockAfterCommande(c);

                        // ajouter le droit de bouchon au solde du compte restaurant
                        rf.addSolde(id_resto, c.getDroitBouchon());

                        // après validation de commande, vider le panier de la session
                        panier.vider();
                        session.setAttribute("panier", panier);

                        // envoyer la confirmation de commande 
                        request.setAttribute("validatedCommande", c);
                        request.getRequestDispatcher("pages/confirm.jsp").forward(request, response);
                    } catch (ParseException ex) {
                    }

                }
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
