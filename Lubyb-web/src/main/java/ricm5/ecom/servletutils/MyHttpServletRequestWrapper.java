package ricm5.ecom.servletutils;

import java.util.Map;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

public class MyHttpServletRequestWrapper extends HttpServletRequestWrapper {

    public MyHttpServletRequestWrapper(ServletRequest request) {
        super((HttpServletRequest) request);
    }
    
    @Override
    public Map<String, String[]> getParameterMap() {
        Map params = super.getParameterMap();
        params.remove("password");
        return params;
    }
}
