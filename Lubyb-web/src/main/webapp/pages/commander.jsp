<%@page import="java.util.Date"%>
<%@page import="java.util.Iterator"%>
<%@page import="ricm5.ecom.entitybean.Restaurant"%>
<%@page import="java.util.List"%>
<%@page import="javax.ejb.EJB"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>    
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Passer la commande</title>
        <link href="/Lubyb-web/bootstrap/css/bootstrap.css" rel="stylesheet">
        <link href="/Lubyb-web/bootstrap/css/bootstrap-timepicker.css" rel="stylesheet">
        <link href="/Lubyb-web/css/page.css" rel="stylesheet">
    </head>
    <body id="body">
        <% List<Restaurant> restos = (List<Restaurant>) request.getAttribute("restaurants");%>
        <div id="page">
            <jsp:include page="../elements/header.jsp">
                <jsp:param name="pagecourante" value="Mon compte"/>
            </jsp:include>
            <div id="breadcrumbs">
                <jsp:include page="../elements/breadcrumbs.jsp">
                    <jsp:param name="number" value="3"/>
                    <jsp:param name="path1" value="/Lubyb-web"/>
                    <jsp:param name="number1" value="Accueil"/>
                    <jsp:param name="path2" value="/Lubyb-web/Panier"/>
                    <jsp:param name="number2" value="Mon Panier"/>
                    <jsp:param name="path3" value="/Lubyb-web"/>
                    <jsp:param name="number3" value="Commande"/>
                </jsp:include>
            </div>
            <div class="inpage">
                <form id="cmdForm" action="ValiderCommande" method="post">
                    <fieldset>
                        <legend>Livraison</legend>
                        <label>Nom :</label> 
                        <input required type='text' name='nomClient'>
                        <label>Prénom :</label> 
                        <input required="" type='text' name='prenomClient'>
                        <label>Adresse de facturation :</label>
                        <input required type='text' name='addr_fact'>
                        <label>Lieu de livraison :</label>
                        <% if (restos != null && !restos.isEmpty()) {
                                for (Iterator it = restos.iterator(); it.hasNext();) {
                                    Restaurant restau = (Restaurant) it.next();
                        %>
                        <label class="radio">
                            <input type="radio" name="restaurant" id="optionsRadios1" value=<% out.println(restau.getSiret()); %> >
                            <% out.println(restau.getName() + " - " + restau.getAdresse());%>
                        </label>
                        <%
                            }
                        } else {%>
                        <div class="alert alert-error">
                            Aucun restaurant partenaire disponible 
                        </div>
                        <%}
                        %>
                        <label>Date de livraison :</label>
                        <input required type='date' name='dateLivraison'/>
                        <label>Heure de livraison :</label>
                        <div class="input-append bootstrap-timepicker-component">
                            <input required name="heure" type="text" class="input-small timepicker-1">
                            <span class="add-on">
                                <i class="icon-time"></i>
                            </span>
                        </div>
                        <label class="checkbox">
                            <input required id="check" type="checkbox"> J'ai pris connaissance des <a href="">conditions générales de vente</a>
                        </label>
                        <div id="alertMissing"class="alert alert-error" style="display: none;">
                            <button type="button" class="close" data-dismiss="alert" onclick="closealertM()">&times;</button>
                            <h4>Attention</h4>
                            Veuillez accepter les conditions générales de ventes
                        </div>
                        <label><span style="color:red">La commande doit être enregistrée au moins 48h avant la livraison</span></label>

                    </fieldset>
                    <div class="form-actions">
                        <input class="btn btn-primary" type="submit" value="Valider">
                        <button onclick="window.history.go(-1)" type="button" class="btn">Annuler</button>
                    </div>
                </form>

            </div>



            <!--//footer-->
            <jsp:include page="../elements/footer.jsp"/>
            <div id="basdepage"></div>
        </div>
    </body>
    
    <script type="text/javascript" src="js/date.js"></script>
    <script type="text/javascript" src="bootstrap/js/bootstrap.js"></script>
    <script type="text/javascript" src="bootstrap/js/bootstrap-timepicker.js"></script>
    <script type="text/javascript" src="/Lubyb-web/js/jquery.js"></script>
    <script type="text/javascript" src="http://jzaefferer.github.com/jquery-validation/jquery.validate.js"></script>
    <script>
    
    
    
    
        $(document).ready(function(){
            
     
        $('#heure').timepicker({
            minuteStep: 5,
            showMeridian: false
        });
        
        function dayDiff(d1, d2){
            d1 = d1.getTime() / 86400000;
            d2 = d2.getTime() / 86400000;
            return new Number(d2 - d1).toFixed(0);
        }

     
     
            
            $("#cmdForm").validate({
                submitHandler: function(form){
                    if($('#check').is(':checked')){
                         form.submit();
                         $('#alertMissing').hide();
                    } else{
                         $('#alertMissing').show();
                    }
                   
                },
                errorElement: "div",
                wrapper: "div",  // a wrapper around the error message
                errorPlacement: function(error, element) {
                    offset = element.offset();
                    error.insertBefore(element)
                    error.addClass('message');  // add a class to the wrapper
                    error.css('position', 'absolute');
                    error.css('left', offset.left + element.outerWidth());
                    error.css('top', offset.top);
                }
            });
        })

        
        jQuery.extend(jQuery.validator.messages, {
            required: "Champs Obligatoire"
        });
    </script>
</html>
