<%@page import="java.util.Map"%>
<%@page import="java.util.HashMap"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Locale"%>
<%@page import="java.util.Locale"%>
<%@page import="java.util.Date"%>
<%@page import="ricm5.ecom.entitybean.Commande"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page import="ricm5.ecom.entitybean.Vin"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    if ((session.getAttribute("userRole") == null) || (!(session.getAttribute("userRole").equals("ADMIN")))) {
        response.sendRedirect("/Lubyb-web/pages/logon.jsp?auth-error=true&redirect=/Lubyb-web/AdminPage");
    }
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Accueil Administration</title>
        <link href="../bootstrap/css/bootstrap.css" rel="stylesheet">
        <link href="../css/page.css" rel="stylesheet">
    </head>
    <body id="body">
        <% List<Commande> listeCmdPending = (List<Commande>) request.getAttribute("listependingcmd");
            List<Commande> listeCmdReady = (List<Commande>) request.getAttribute("listereadycmd");%>
        <div id="page">
            <jsp:include page="../elements/headerAdmin.jsp">
                <jsp:param name="pagecourante" value="accueil"/>
            </jsp:include>
            <div>
                <jsp:include page="../elements/breadcrumbs.jsp">
                    <jsp:param name="number" value="2"/>
                    <jsp:param name="path1" value="/Lubyb-web"/>
                    <jsp:param name="number1" value="Accueil"/>
                     <jsp:param name="path2" value="/Lubyb-web/AdminPage"/>
                    <jsp:param name="number2" value="Administration"/>
                </jsp:include>
            </div>

            <div class="inpage">
                <fieldset>
                    <legend>Commandes prêtes à être livrées</legend>
                    <%
                        if (listeCmdReady != null && !listeCmdReady.isEmpty()) {%>
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <td>N°</td>
                                <td>Date</td>
                                <td>Heure</td>
                                <td>Lieu de livraison</td>
                                <td>Action</td>
                            </tr>
                        </thead>
                        <tbody>
                            <%
                                DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                                DateFormat timeFormat = new SimpleDateFormat("hh:mm");
                                for (Iterator it = listeCmdReady.iterator(); it.hasNext();) {
                                    Commande cmd = (Commande) it.next();
                                    Date dateAndTime = cmd.getDateToDeliver();
                                    String date = dateFormat.format(dateAndTime);
                                    String time = timeFormat.format(dateAndTime);
                            %>
                            <tr>
                                <td><% out.println(cmd.getId());%></td>
                                <td><% out.println(date);%></td>
                                <td><% out.println(time);%></td>
                                <td><a target="_blank" href="http://maps.google.fr/maps?f=q&hl=fr&q=<% out.println(cmd.getRestaurant().getAdresse());%>">Voir la carte</a></td>
                                <td></td>
                            </tr>
                            <%
                                }
                            %>
                        </tbody>
                    </table>
                    <%                    } else {%>
                    <div class="alert alert-info">
                        Il n'y a actuellement pas de commande prête à être livrée !
                    </div>
                    <%}
                    %>
                </fieldset>
                <fieldset>
                    <legend>Commandes à préparer</legend>
                    <%
                        if (listeCmdPending != null && !listeCmdPending.isEmpty()) {%>
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <td>N°</td>
                                <td>Date</td>
                                <td>Heure</td>
                                <td>Produits</td>
                                <td>Action</td>
                            </tr>
                        </thead>
                        <tbody>
                            <%
                                DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                                DateFormat timeFormat = new SimpleDateFormat("hh:mm");
                                for (Iterator it = listeCmdPending.iterator(); it.hasNext();) {
                                    Commande cmd = (Commande) it.next();
                                    Date dateAndTime = cmd.getDateToDeliver();
                                    String date = dateFormat.format(dateAndTime);
                                    String time = timeFormat.format(dateAndTime);
                            %>
                            <tr>
                                <td><% out.println(cmd.getId());%></td>
                                <td><% out.println(date);%></td>
                                <td><% out.println(time);%></td>
                                <td><table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <td><b>Ref</b></td>
                                                <td><b>Quantité</b></td>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <% 
                                                for (Iterator itProd = cmd.getProduits().entrySet().iterator(); itProd.hasNext();) {
                                                    Map.Entry entry = (Map.Entry) itProd.next();
                                                    Vin v = (Vin) entry.getKey();
                                                    Integer quantite = (Integer) entry.getValue();
                                            %>
                                            <tr>
                                                <td><% out.println(v.getId());%></td>
                                                <td><% out.println(quantite);%></td>
                                            </tr>
                                            <% }
                                            %>
                                        </tbody>
                                    </table>
                                </td>
                                <td><button class="btn btn-success" disabled>Prête !</button></td>
                            </tr>
                            <%
                                }
                            %>
                        </tbody>
                    </table>
                    <%                    } else {%>
                    <div class="alert alert-info">
                    Il n'y a actuellement pas de commandes en attente de préparation !
                    </div>
                    <%}
                    %>
                </fieldset>
            </div>
            <jsp:include page="../elements/footer.jsp"/>
            <div id="basdepage"></div>
        </div>
    </body>
    <script src="/Lubyb-web/js/jquery.js"></script>
</html>
