<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Inscription</title>
    </head>
    <% String loginUsed = (String) request.getAttribute("loginused");%>
    <body id="body">
        <div id="page">
            <jsp:include page="../elements/header.jsp">
                <jsp:param name="pagecourante" value=""/>
            </jsp:include>
            <div>
                <jsp:include page="../elements/breadcrumbs.jsp">
                    <jsp:param name="number" value="2"/>
                    <jsp:param name="path1" value="/Lubyb-web"/>
                    <jsp:param name="number1" value="Accueil"/>
                    <jsp:param name="path2" value="/Lubyb-web/Inscription"/>
                    <jsp:param name="number2" value="Inscription"/>
                </jsp:include>
            </div>

            <div class="inpage">
                <form id="inscription" method="post" >
                    <fieldset>
                        <legend>Informations de compte :</legend>
                        <label>Login :</label>
                        <input required type="text" id="login" name="login" type="text" placeholder="Ex : Yeti38">
                        <% if (loginUsed != null) {%>
                        <div id="alertLog"class="alert alert-error"">
                             <button type="button" class="close" data-dismiss="alert" onclick="closealertL()">&times;</button>
                            <h4>Attention</h4>
                            Le nom d'utilisateur <% out.println(loginUsed);%> est déjà utilisé. Merci d'en choisir un autre
                        </div>
                        <%}%>
                        <label>Mot de passe :</label>
                        <input required id="password" name="password" type="password" placeholder="Ex : MotDepasse47éè">
                        <label>Confirmer le mot de passe :</label>
                        <input required id="passwordconf" type="password" placeholder="Ex : MotDepasse47éè" equalTo="#password">
                        <div id="alertPsw"class="alert alert-error" style="display: none;">
                            <button type="button" class="close" data-dismiss="alert" onclick="closealertP()">&times;</button>
                            <h4>Attention</h4>
                            Les mots de passe doivent correspondre
                        </div>
                    </fieldset>
                    <fieldset>
                        <legend>Informations personnelles :</legend>
                        <label>Nom :</label>
                        <input required type="text" id="nom" name="nom" type="text" placeholder="Ex : Valjean">
                        <label>Prenom :</label>
                        <input required type="text" id="prenom" name="prenom" type="text" placeholder="Ex : Jean">
                        <label>Téléphone :</label>
                        <input required type="digits" id="tel" name="telephone" placeholder="Ex : 0102030405">
                        <label>E-mail :</label>
                        <input required type="email" name="mail" placeholder="Ex : monMail@wanadoo.fr">
                        <label>Adresse de facturation :</label>
                        <input required name="adresse_fact" type="text" placeholder="Ex : 11 rue de la poste 38000 Grenoble">
                        <label class="checkbox">
                            <input required id="check" type="checkbox"> J'ai pris connaissance des <a href="">conditions d'utilisation</a>
                        </label>
                        <div id="alertMissing"class="alert alert-error" style="display: none;">
                            <button type="button" class="close" data-dismiss="alert" onclick="closealertM()">&times;</button>
                            <h4>Attention</h4>
                            Veuillez accepter les conditions d'utilisation
                        </div>
                        <div class="form-actions">
                            <button id="submitForm" type="submit" class="btn btn-primary">S'inscrire</button>
                            <button id="cancel" onclick="window.history.go(-1)" class="btn">Annuler</button>
                        </div>
                    </fieldset>
                </form>
            </div>
            <jsp:include page="../elements/footer.jsp"/>
            <div id="basdepage"></div>
        </div>
    </body>
    <script src="/Lubyb-web/js/jquery.js"></script>
    <script type="text/javascript" src="http://jzaefferer.github.com/jquery-validation/jquery.validate.js"></script>

    <script>
        
        $(document).ready(function(){
            
            
            $("#inscription").validate({
                submitHandler: function(form){
                    if($('#check').is(':checked')){
                         form.submit();
                         $('#alertMissing').hide();
                    } else{
                         $('#alertMissing').show();
                    }
                   
                },
                errorElement: "div",
                wrapper: "div",  // a wrapper around the error message
                errorPlacement: function(error, element) {
                    offset = element.offset();
                    error.insertBefore(element)
                    error.addClass('message');  // add a class to the wrapper
                    error.css('position', 'absolute');
                    error.css('left', offset.left + element.outerWidth());
                    error.css('top', offset.top);
                }
            });
        })

        
        jQuery.extend(jQuery.validator.messages, {
            required: "Champs Obligatoire",
            email: "Email Non-Valide",
            digits: "Contenu Incorrect",
            equalTo: "Champs Diff&eacute;rents"
        });
        

        
        function closealertM(){
            $('#alertMissing').hide();
        }
        
        function closealertP(){
            $('#alertPsw').hide();
        }
        
        function closealertL(){
            $('#alertLog').hide();
        }
        
    </script>
</html>
