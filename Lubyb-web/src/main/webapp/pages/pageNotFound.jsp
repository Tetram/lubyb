
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>    
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>404 Page introuvable</title>
        <link href="../bootstrap/css/bootstrap.css" rel="stylesheet">
        <link href="../css/page.css" rel="stylesheet">
    </head>
    <body id="body">
        <div id="page">
            <jsp:include page="../elements/header.jsp">
                <jsp:param name="pagecourante" value="Confirmation de commande"/>
            </jsp:include>

            
            <div class="inpage"> 
                <div class="alert alert-error" style="text-align: center; margin:0em 20em 0em 20em">
                    <h4>404 - Produit introuvable</h4>
                </div><br>
                <div style="text-align: center">
                    <img src="/Lubyb-web/images/404.jpg" class="img-polaroid">
                </div>
            </div>
            
            <!--//footer-->
            <jsp:include page="../elements/footer.jsp"/>
            <div id="basdepage"></div>
            
        </div>
    </body>
</html>
