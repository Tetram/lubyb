<%@page import="java.util.HashMap"%>
<%@page import="java.util.Map"%>
<%@page import="java.util.List"%>
<%@page import="ricm5.ecom.entitybean.Vin"%>
<%@page import="ricm5.ecom.sessionbean.PanierClient"%>
<%@page import="java.util.Iterator"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>    
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Panier</title>
        <link href="/Lubyb-web/bootstrap/css/bootstrap.css" rel="stylesheet">
        <link href="/Lubyb-web/css/page.css" rel="stylesheet">
    </head>
    <body id="body">
        <div id="page">
            <jsp:include page="../elements/header.jsp">
                <jsp:param name="pagecourante" value="Mon compte"/>
            </jsp:include>
            <div id="breadcrumbs">
                <jsp:include page="../elements/breadcrumbs.jsp">
                    <jsp:param name="number" value="2"/>
                    <jsp:param name="path1" value="/Lubyb-web"/>
                    <jsp:param name="number1" value="Accueil"/>
                    <jsp:param name="path2" value="/Lubyb-web/Panier"/>
                    <jsp:param name="number2" value="Panier"/>
                </jsp:include>
            </div>
            
            <!-- Recuperer la liste de vins-->
            <%  PanierClient panier = (PanierClient) session.getAttribute("panier");
                Vin v;
                Integer quantite;
                Integer ajoutOK = (Integer) request.getAttribute("ajoutOK");
                Map.Entry entry;
                Double prixtotal = panier.prixTotal();
                Iterator it = panier.getListeAchats().entrySet().iterator();%>
                
                <%if(ajoutOK == 0){%>
                    <div id="alertMissing" class="alert alert-error" style="margin:0em 10em 0em 10em">
                        <button type="button" class="close" data-dismiss="alert" onclick="closealert()">&times;</button>
                        <strong>Attention!</strong> Quantité demandée dépasse le stock.
                    </div>
                <%}else{
                    if(ajoutOK == -1){%>
                        <div id="alertMissing" class="alert alert-error" style="margin:0em 10em 0em 10em">
                            <button type="button" class="close" data-dismiss="alert" onclick="closealert()">&times;</button>
                            <strong>Attention!</strong> Produit en rupture de stock.
                        </div>
                    <%}
                }%>
                
                <%if(!it.hasNext()){%>
                     <!--// le panier est vide-->
                     <div class="pager"><h>Votre panier est vide </h></div>
                <%}else{%>
                    
                
                    <div class="inpage">
                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th style="width:10%"></th>
                                <th style="width:50%">Désignation</th>
                                <th style="width:12%">PRIX U. TTC  </th>
                                <th style="width:15%">Quantité     </th>
                                <th style="width:10%">Total TTC    </th>
                                <th style="width:3%"></th>
                            </tr>
                        </thead>
                    <%while(it.hasNext()){
                        entry = (Map.Entry) it.next();
                        v = (Vin) entry.getKey();
                        quantite = (Integer) entry.getValue();%>
                        <tbody>
                            <tr>
                                <td style="vertical-align:middle" >
                                    <img src="<%out.println(v.getImageUri()); %>"/>
                                </td>
                                <td style="vertical-align:middle">
                                    <a href=""> <%out.println(v.getNom());%> </a><br>
                                    <span> <% out.println(v.getDescription()); %> </span>
                                </td>
                                <td style="vertical-align:middle"> 
                                    <%out.println(v.getPrix()); %> &nbsp;&euro;
                                </td>
                                <td style="vertical-align:middle"> 
                                    <div>
                                        <input class="span1"  name="quantite" type="text" value="<%out.println(quantite); %>"/> 
                                        <form style="display:inline;" action="Panier" method="post" > 
                                            <input type='hidden' name='id' value=<%out.println(v.getId());%>>
                                            <input type="image" src="/Lubyb-web/images/moins.gif" name="mode" value="decrement" alt="Submit">
                                            <input type="image" src="/Lubyb-web/images/plus.gif" name="mode" value="increment" alt="Submit"> 
                                        </form> 
                                    </div> 
                                </td> 
                                <td style="vertical-align:middle"> 
                                     <%out.println(v.getPrix() * quantite); %> &nbsp;&euro;
                                </td> 
                                <td style="vertical-align:middle"> 
                                    <form action="Panier" method="post"> 
                                        <input type='hidden' name='id' value=<%out.println(v.getId());%>>
                                        <input class="btn" type="image" src="/Lubyb-web/images/trash2.png" name="mode" value="remove" alt="Submit"> 
                                    </form> 
                                </td>
                            </tr>
                      <%}%>
                            <!--// Prix total du panier :--> 
                            <tr style="color:blue"> 
                                <th></th> <th></th> <th></th> <th>Prix total TTC</th> 
                                <th> <%out.println(prixtotal); %> &nbsp;&euro;</th> <th></th>
                            </tr> 
                        </tbody> 
                    </table> 
                    </div>
                            
                    <a id="cancel" href="/Lubyb-web/Vins" class="btn" style="float:left;margin: 0 20px 0;">Continuer Achat</a>       
                    <!-- Button pour valider la commande:-->
                    <div style="margin-left:55em">
                         <form action="ValiderCommande" method="post" >
                            <input class="btn btn-primary " type="submit" value="Valider la commande">
                          </form>
                    </div>
                <%}%>

            <!--//footer-->
            <jsp:include page="../elements/footer.jsp"/>
            <div id="basdepage"></div>
        </div>
            <% request.setAttribute("ajoutOK", -100); %>
    </body>
    <script src="/Lubyb-web/js/jquery.js"></script>
    <script>
        function closealert(){
        $('#alertMissing').hide();
    }
    </script>
</html>
