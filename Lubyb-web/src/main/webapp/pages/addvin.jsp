<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Ajout catalogue</title>
    </head>
    <body id="body">
        <div id="page">
            <jsp:include page="../elements/header.jsp">
                <jsp:param name="pagecourante" value=""/>
            </jsp:include>
            <div>
                <jsp:include page="../elements/breadcrumbs.jsp">
                    <jsp:param name="number" value="2"/>
                    <jsp:param name="path1" value="/Lubyb-web"/>
                    <jsp:param name="number1" value="Accueil"/>
                    <jsp:param name="path2" value="/Lubyb-web/Inscription"/>
                    <jsp:param name="number2" value="Ajout vin"/>
                </jsp:include>
            </div>

            <div class="inpage">
                <form id="inscription" method="post">
                    <fieldset>
                        <label>Référence :</label>
                        <input id="reference" name="reference" type="text" placeholder="Ex : 0102030405" oninput="checkForm()">
                        
                        <label>Nom :</label>
                        <input id="nom" name="nom" type="text" placeholder="Ex : Armagnac" oninput="checkForm()">
                        
                        <label>Region :</label>
                        <input id="region" name="region" type="text" placeholder="Ex : Isere" oninput="checkForm()">

                        <label>Année :</label>
                        <input id="annee" name="annee" type="text" placeholder="Ex : 2012" oninput="checkForm()">
                        
                        <label>Couleur :</label>
                        <select name="couleur"><option value="ROUGE">Rouge</option>"
                    + "<option value="ROSE">Rosé</option>"
                    + "<option value="BLANC">Blanc</option>"
                    + "<option value="JAUNE">Jaune</option>"
                    + "</select>;
                    
                        <label>Prix :</label>
                        <input id="prix" name="prix" type="text" placeholder="Ex : 150" oninput="checkForm()">
                         
                        <label>Stock :</label>
                        <input id="stock" name="stock" type="text" placeholder="Ex : 15" oninput="checkForm()">
                        
                        <label>URL image :</label>
                        <input id="url" name="url" type="text" placeholder="Ex : " oninput="checkForm()">
                        
                        <label>
                        </label>
                        
                        <div class="form-actions">
                            <button id="submitForm" type="submit" class="btn" disabled="true" >Enregistrer</button>
                            <button type="button" class="btn" onclick="window.location.replace('DeleteVin')">Annuler</button>
                        </div>
                    </fieldset>
                </form>
            </div>
            <jsp:include page="../elements/footer.jsp"/>
            <div id="basdepage"></div>
        </div>
    </body>
    <script src="/Lubyb-web/js/jquery.js"></script>
    <script>
        function checkForm(){
            $('#submitForm').attr('disabled', 'true');
            //Tester les champs !
            ok = true;
            
            if($('#reference').val() == '') {ok = false;}
            
            if($('#nom').val() == '') {ok = false;}
            
            if($('#annee').val() == '') {ok = false;}
            
            if($('#prix').val() == '') {ok = false;}
            
            if($('#region').val() == '') {ok = false;}
            
            if($('#stock').val() == '') {ok = false;}
          
            if(ok){
            $('#submitForm').removeAttr("disabled");}
        }
        
    </script>
</html>
