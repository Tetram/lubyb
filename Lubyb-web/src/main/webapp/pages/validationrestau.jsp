<%@page import="java.util.Iterator"%>
<%@page import="ricm5.ecom.entitybean.Restaurant"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%
    if((session.getAttribute("userRole") == null) || (!(session.getAttribute("userRole").equals("ADMIN")))){
        response.sendRedirect("/Lubyb-web/pages/logon.jsp?auth-error=true&redirect=/Lubyb-web/AdminPage/ValidationPartenariat");
    }
%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body id="body">
        <% List<Restaurant> listeRestaus = (List<Restaurant>) request.getAttribute("pendingRestau");
            String lastAction = request.getParameter("done");
            String doneOn = request.getParameter("siret");
        %>
        <div id="page">
            <jsp:include page="../elements/headerAdmin.jsp">
                <jsp:param name="pagecourante" value="validation"/>
            </jsp:include>
            <div>
                <jsp:include page="../elements/breadcrumbs.jsp">
                    <jsp:param name="number" value="3"/>
                    <jsp:param name="path1" value="/Lubyb-web"/>
                    <jsp:param name="number1" value="Accueil"/>
                    <jsp:param name="path2" value="/Lubyb-web/AdminPage"/>
                    <jsp:param name="number2" value="Administration"/>
                    <jsp:param name="path3" value="/Lubyb-web/AdminPage/ValidationPartenariat"/>
                    <jsp:param name="number3" value="Partenariats en attente"/>
                </jsp:include>
            </div>

            <div class="inpage">
                <% if (lastAction != null && doneOn != null) {%>
                <div id="alert"class="alert alert-success">
                    <button type="button" class="close" data-dismiss="alert" onclick="closealert()">&times;</button>
                    <h4>Action réalisée avec succès</h4>
                    Le restaurant a été <% out.println(lastAction); %> comme nouveau partenaire (N° siret : <% out.println(doneOn); %>)
                </div>
                <%                    }
                %>
                Demandes de partenariat en attente de validation :
                <%
                    if (listeRestaus != null && !listeRestaus.isEmpty()) {%>
                <table class="table">
                    <thead>
                        <tr>
                            <td>#</td>
                            <td>N° Siret</td>
                            <td>Nom</td>
                            <td>Addresse</td>
                        </tr>
                    </thead>
                    <tbody>
                        <%
                            int i = 0;
                            for (Iterator it = listeRestaus.iterator(); it.hasNext();) {
                                Restaurant restau = (Restaurant) it.next();
                        %>
                        <tr>
                            <td><% out.println(i++);%></td>
                            <td><% out.println(restau.getSiret());%></td>
                            <td><% out.println(restau.getName());%></td>
                            <td><% out.println(restau.getAdresse());%></td>
                            <td><a href="/Lubyb-web/AdminPage/AcceptRestau?siret=<% out.println(restau.getSiret());%>"><button class="btn btn-success">Valider</button></a>  <a><button class="btn btn-danger">Refuser</button></a></td>
                        </tr>
                        <%
                            }
                        %>
                    </tbody>
                </table>
                <%                        } else {%>
                <div class="alert alert-info">
               Il n'y a pas de demandes en attente.
               </div>
                <%}
                %>
            </div>
            <jsp:include page="../elements/footer.jsp"/>
            <div id="basdepage"></div>
        </div>
    </body>
    <script src="/Lubyb-web/js/jquery.js"></script>
    <script>
        function closealert() {
            $('#alert').hide();
        }
    </script>
</html>
