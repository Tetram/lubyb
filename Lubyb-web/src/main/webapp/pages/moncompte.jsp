<%@page import="ricm5.ecom.entitybean.Utilisateur.Role"%>
<%@page import="ricm5.ecom.entitybean.Utilisateur"%>
<%@page import="java.util.Iterator"%>
<%@page import="ricm5.ecom.entitybean.Restaurant"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Mon compte</title>
        <link href="/Lubyb-web/bootstrap/css/bootstrap.css" rel="stylesheet">
        <link href="/Lubyb-web/css/page.css" rel="stylesheet">
    </head>
    <% List<Restaurant> listeRestaus = (List<Restaurant>) request.getAttribute("mesRestaus");
        Utilisateur me = (Utilisateur) request.getAttribute("me");
    %>
    <body id="body">
        <div id="page">
            <jsp:include page="../elements/header.jsp">
                <jsp:param name="pagecourante" value="Mon compte"/>
            </jsp:include>
            <div id="breadcrumbs">
                <jsp:include page="../elements/breadcrumbs.jsp">
                    <jsp:param name="number" value="2"/>
                    <jsp:param name="path1" value="/Lubyb-web"/>
                    <jsp:param name="number1" value="Accueil"/>
                    <jsp:param name="path2" value="/Lubyb-web/pages/moncompte.jsp"/>
                    <jsp:param name="number2" value="Mon compte"/>

                </jsp:include>
            </div>
            <div class="inpage">
                <div class="tabbable tabs-left">
                    <ul class="nav nav-tabs">
                        <li id="info" class="active">
                            <a onclick="info()">Informations personnelles</a>
                        </li>
                        <li id="mdp"><a onclick="mdp()">Changer mon mot de passe</a></li>
                        <li id="restau"><a onclick="restau()">Restaurateur</a></li>
                        <% 
                        if(me.isAdmin()) {out.println("<li id='Stock'><a onclick='stock()'>Administration</a></li>");}
                       
                        %>
                        
                    </ul>
                    <div id="ongletInfo" class="tab-content">
                        <fieldset>
                            <legend>Mes informations personnelles</legend>
                            <label>Nom : <% out.println(me.getNom());%></label>
                            <label>Prenom : <% out.println(me.getPrenom());%></label>
                            <label>Téléphone : <% out.println(me.getTelephone());%></label>
                            <label>E-mail : <% out.println(me.getEmail());%></label>
                            <label>Adresse de facturation : <% out.println(me.getAdresseFact());%></label>
                        </fieldset>
                    </div>
                    <div id="ongletMdp" style="display: none;" class="tab-content">
                        <form id="modifmdp">
                            <fieldset>
                                <legend>Changer mon mot de passe</legend>
                                <label>Nouveau mot de passe</label>
                                <input id="mdp1" name="mdp1" type="password" placeholder="Ex : MotDePasse12">
                                <label>Nouveau mot de passe (vérification)</label>
                                <input id="mdp2" name="mdp2" type="password" placeholder="Ex : MotDePasse12">
                                <label></label>

                            </fieldset>
                            <div class="form-actions">
                                <button id="submitFormMdp" type="submit" class="btn btn-primary" disabled="true">Changer</button>
                            </div>
                        </form>
                    </div>
                    <div id="ongletRestau" style="display: none;" class="tab-content">
                        <div class="breadcrumb">
                            <li id="lignePres">
                                Vous êtes restaurateur et souhaitez ajouter un restaurant
                                partenaire de notre site ? 
                                <a onclick="showform()">Cliquez ici</a>
                            </li>
                            <form id="ajoutrestau" style="display: none;" onsubmit="return checkForm()">
                                <fieldset>
                                    <legend>Demander un partenariat</legend>
                                    <label>N° Siret du restaurant :</label>
                                    <input id="siret" name="siret" type="text" placeholder="Ex : 12345678912345">
                                    <label>Nom du restaurant :</label>
                                    <input id="nom" name="nom" type="text" placeholder="Ex : La bonne fourchette">
                                    <label>Adresse du restaurant :</label>
                                    <input id="address" name="adresse" type="text" placeholder="Ex : 11 rue Peri 38100 Grenoble">
                                    <label class="checkbox" onclick="checked()">
                                        <input id="check" type="checkbox"> J'ai pris connaissance des <a>conditions de partenariat</a>
                                    </label>
                                    <div id="alertMissing" class="alert alert-error" style="display: none;">
                                        <button type="button" class="close" data-dismiss="alert" onclick="closealertM()">&times;</button>
                                        <h4>Attention</h4>
                                         Veuillez accepter les conditions de partenariat
                                    </div>

                                </fieldset>
                                <div class="form-actions">
                                    <button id="submitForm" type="submit" class="btn btn-primary" >Demander un partenariat</button>
                                </div>
                            </form>
                        </div>

                        <h3>Vos restaurants :</h3>

                        <%
                        if (listeRestaus != null && !listeRestaus.isEmpty()) {%>
                        <table class="table">
                            <thead>
                                <tr>
                                    <td>#</td>
                                    <td>N° Siret</td>
                                    <td>Nom</td>
                                    <td>Solde</td>
                                    <td>Status</td>
                                </tr>
                            </thead>
                            <tbody>
                                <%
                                    int i = 0;
                                    for (Iterator it = listeRestaus.iterator(); it.hasNext();) {
                                        Restaurant restau = (Restaurant) it.next();
                                %>
                                <tr>
                                    <td><% out.println(i++);%></td>
                                    <td><% out.println(restau.getSiret());%></td>
                                    <td><% out.println(restau.getName());%></td>
                                    <td><% out.println(restau.getSolde());%> €</td>
                                    <td><% if (restau.getStatus().equals(Restaurant.Status.PENDING)) {
                                            out.println("En attente de validation");
                                        } else if (restau.getStatus().equals(Restaurant.Status.VALIDATED)) {
                                            out.println("Valide");
                                        } else {
                                            out.println("Erreur, Veuillez contacter un admin");
                                        }%></td>
                                </tr>
                                <%
                                    }
                                %>
                            </tbody>
                        </table>
                        <%                        } else {%>
                        <div class="alert alert-help"> 
                        Vous n'avez pas de restaurants partenaires.
                        </div>
                        <%}
                        %>
                    </div>
                    
                   <div id="ongletStock" style="display: none;" class="tab-content">
                        <form action=AdminPage/AddVin method='post'>
                            <fieldset>
                                <td> <input class="btn btn-primary" type='submit' value='Ajouter un vin'> </td>
                            </fieldset>
                        </form> 
                       
                       <form action=AdminPage/DeleteVin method='post'>
                            <fieldset>
                                <td> <input class="btn btn-primary" type='submit' value='Supprimer un vin'> </td>
                            </fieldset>
                        </form> 
                    </div>
                </div>
            </div>
            <jsp:include page="../elements/footer.jsp"/>
            <div id="basdepage"></div>
        </div>
    </div>
</body>
<script src="/Lubyb-web/js/jquery.js"></script>
<script>
    function info(){
        $('#restau').removeClass("active");
        $('#mdp').removeClass("active");
        $('#Stock').removeClass("active");
        $('#info').addClass("active");
        $('#ongletMdp').hide();
        $('#ongletRestau').hide();
        $('#ongletStock').hide();
        $('#ongletInfo').show();
    }
    
    function mdp(){
        $('#restau').removeClass("active");
        $('#info').removeClass("active");
        $('#Stock').removeClass("active");
        $('#mdp').addClass("active");
        $('#ongletInfo').hide();
        $('#ongletRestau').hide();
        $('#ongletStock').hide();
        $('#ongletMdp').show();
    }
    
    function restau(){
        $('#info').removeClass("active");
        $('#mdp').removeClass("active");
        $('#Stock').removeClass("active");
        $('#restau').addClass("active");
        $('#ongletMdp').hide();
        $('#ongletInfo').hide();
        $('#ongletStock').hide();
        $('#ongletRestau').show();
    }
    
    function stock(){
       window.location.replace('AdminPage')
    }
    
    function showform(){
        $('#lignePres').hide();
        $('#ajoutrestau').show();
    }
    
    function checkForm(){
        //Tester les champs !
        ok = true;
        red = '#FF9187';
        white = '#FFFFFF';
        $('#nom').css('background-color', white);
        $('#siret').css('background-color', white);
        $('#address').css('background-color', white);
        if($('#nom').val() == ''){
            ok = false;
            $('#nom').css('background-color', red);
        }
        if($('#address').val() == ''){
            ok = false;
            $('#address').css('background-color', red);
        }
        if($('#siret').val() == ''){
            ok = false;
            $('#siret').css('background-color', red);
        }
        
        if(!ok){
            $('#alertMissing').show();
        }
        //Test de checkbox
        if(!$('#check').attr('checked')){
            ok = false;
            $('#alertMissing').show();
        }
        
        return ok;
    }
    
    function checked(){
        if($('#check').attr('checked')){
            $('#submitForm').removeAttr("disabled");
        }
    }
    
    function closealertM(){
        $('#alertMissing').hide();
    }
</script>
</html>
