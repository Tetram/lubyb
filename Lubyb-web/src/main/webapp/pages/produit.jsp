
<%@page import="ricm5.ecom.entitybean.Vin"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Produit</title>
        <link href="/Lubyb-web/bootstrap/css/bootstrap.css" rel="stylesheet">
        <link href="/Lubyb-web/css/page.css" rel="stylesheet">
    </head>
    <body id="body">
        <div id="page">
            <jsp:include page="../elements/header.jsp">
                <jsp:param name="pagecourante" value="Produit"/>
            </jsp:include>
            <div id="breadcrumbs">
                <jsp:include page="../elements/breadcrumbs.jsp">
                    <jsp:param name="number" value="2"/>
                    <jsp:param name="path1" value="/Lubyb-web"/>
                    <jsp:param name="number1" value="Accueil"/>
                    <jsp:param name="path2" value="/Lubyb-web/"/>
                    <jsp:param name="number2" value="Produit"/>
                </jsp:include>
            </div>
            
            <%
            // recupérer ID du produit pour afficher
            String id = (String) request.getParameter("id");
            System.out.println("produitInfo.jsp : ID = "+ id);
            Vin v = (Vin) request.getAttribute("vin");
            if(v == null){
                request.getRequestDispatcher("pageNotFound.jsp").forward(request, response);
            }else{%>
            
                <div class="inpage">
                    <div id="produitInfoBlock" class="clearfix">
                        <div class="page-header">
                            <h1>Hello</h1>
                        </div>

                    </div>
                </div>
                
            <%} %>
                    

             <!--//footer-->
            <jsp:include page="../elements/footer.jsp"/>
            <div id="basdepage"></div>
        </div>
    </body>
</html>
