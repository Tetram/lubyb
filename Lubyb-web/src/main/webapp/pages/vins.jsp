<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page import="ricm5.ecom.entitybean.Vin"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Accueil Lubyb</title>
        <link href="bootstrap/css/bootstrap.css" rel="stylesheet">
        <link href="css/page.css" rel="stylesheet">
    </head>
    <body id="body">
        <% List<Vin> liste = (List<Vin>) request.getAttribute("liste");%>
        <div id="page">
            <jsp:include page="../elements/header.jsp">
                <jsp:param name="pagecourante" value="vins"/>
            </jsp:include>
            <div>
                <jsp:include page="../elements/breadcrumbs.jsp">
                    <jsp:param name="number" value="2"/>
                    <jsp:param name="path1" value="/Lubyb-web"/>
                    <jsp:param name="number1" value="Accueil"/>
                    <jsp:param name="path2" value="/Lubyb-web/SearchVin"/>
                    <jsp:param name="number2" value="Vins"/>
                </jsp:include>
            </div>
            <div class="grillecontainer">

                <% if (liste.size() > 0) {
                        out.println("<ul class=\"thumbnails\">");%>
                <jsp:include page="../elements/recherche.jsp"/>
                <ul style="list-style: none;"><li class="span9">
                    <%
                        for (Iterator it = liste.iterator(); it.hasNext();) {
                            Vin elem = (Vin) it.next();
                            out.println("<li class=\"span3\">");
                            out.println("<div class=\"thumbnail\">");
                            out.println("<img class=\"imgInGrille\" src=\"" + elem.getImageUri() + "\"/>");
                            out.println("<div class=\"product\">");
                            out.println("<a href=#" + elem.getId() + " role='button' data-toggle=\"modal\">Details</a>");
                            out.println("</div>");
                            out.println("<div class=\"caption\">");
                            out.println("<div class=\"descriptif\">");
                            out.println("<h4>" + elem.getNom() + "</h4>");
                            out.println("<p>" + "Ref : " + elem.getId() + "</p>");
                            out.println("<p>" + "Région : " + elem.getRegion().getId() + "</p>");
                            out.println("<p>" + "Couleur : " + elem.getType() + "</p>");
                            out.println("<p>" + "Prix : " + elem.getPrix() + " euros </p>");
                            out.println("</div>");
                            out.println("<form action=\"Panier\" method='post' >");
                            out.println("<td> <input type='hidden' value=" + elem.getId() + " name='id'> </td> ");
                            out.println("<td> <input type='hidden' value=addtocard name='mode'> </td> ");
                            out.println("<td> <input class=\"btn btn-primary\" type='submit' value='Ajouter au panier'> </td> ");
                            out.println("</form>");
                            out.println("</div>");
                            out.println("</div>");%>
                    </li>
               
                <div id='<%= elem.getId()%>' class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="<%= elem.getId()%>Label" aria-hidden="true">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                        <h3 id="<%= elem.getId()%>Label"><%= elem.getNom()%></h3>
                    </div>
                    <div class="modal-body">
                        <img class="imgInGrille" src="<%= elem.getImageUri()%>">
                        <p>Issu d'une très belle récolte et d'un millésime reconnu,</p>
                        <p>Le <b><%= elem.getNom()%> <%= elem.getAnnee()%></b> est un vin d'exception.</p>
                        <p>Typique de la région <%= elem.getRegion()%>, il se démarque par sa robe <%= elem.getType()%> et des notes fruitées </p>
                        <p>Il accompagnera à merveille vos plats, vos desserts et vos apéritifs<p>
                    </div>
                    <div class="modal-footer">
                        <button class="btn" data-dismiss="modal" aria-hidden="true">Fermer</button>
                    </div>
                </div>
                <%
                    }
                    out.println("</ul>");

                } else {
                    out.println("<ul class=\"thumbnails\">");%>
                <jsp:include page="../elements/recherche.jsp"/>
                <li>Pas de vin correspondant trouvé</li>
                <%
                        out.println("</ul>");
                    }%>
                </li></ul>   
                </ul>
             
            </div>
            <jsp:include page="../elements/footer.jsp"/>
            <div id="basdepage"></div>
        </div>
    </body>
    <script src="/Lubyb-web/js/jquery.js"></script>
    <script src="/Lubyb-web/bootstrap/js/bootstrap.js"></script>
    <script></script>
</html>
