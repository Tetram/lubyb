<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page import="ricm5.ecom.entitybean.Vin"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Accueil Lubyb</title>
        <link href="/Lubyb-web/bootstrap/css/bootstrap.css" rel="stylesheet">
        <link href="/Lubyb-web/css/page.css" rel="stylesheet">
    </head>
    <body id="body">
        <% List<Vin> listeMeilleurs = (List<Vin>) request.getAttribute("meilleursVentes");%>
        <% List<Vin> listeNouveaux = (List<Vin>) request.getAttribute("nouveauxVins");%>
        <div id="page">
            <jsp:include page="../elements/header.jsp">
                <jsp:param name="pagecourante" value="accueil"/>
            </jsp:include>
            <div>
                <jsp:include page="../elements/breadcrumbs.jsp">
                    <jsp:param name="number" value="1"/>
                    <jsp:param name="path1" value="/Lubyb-web"/>
                    <jsp:param name="number1" value="Accueil"/>
                </jsp:include>
            </div>

            <div id="myCarousel" class="carousel slide inpage">
                <!-- Carousel items -->
                <div class="carousel-inner">
                    <div class="active item">
                        <img src="/Lubyb-web/images/banniere_carou_1.jpg" >
                        <div class="carousel-caption">
                            <h4>Bienvenue sur Lubyb </h4>
                            <p>   
                                <i>Let Us Bring Your Bottle est un site de vente en ligne  
                                    de vin pour la ville de Grenoble.</i><br>   
                                Notre concept : Vous permettre de profiter du droit du bouchon dans nos restaurants partenaires. 
                            </p> 

                        </div>
                    </div>
                    <div class="item">
                        <img src="/Lubyb-web/images/banniere_carou_2_bis.jpg">
                        <div class="carousel-caption">
                            <h4>Nos Vins</h4>
                            <p>Découvrez notre catalogue de vins et faîtes votre choix parmi un large choix de produits</p>
                        </div>
                    </div>
                    <div class="item">
                        <img src="/Lubyb-web/images/banniere_carou_3_bis.jpg"  width="100%" height="100%" >
                        <div class="carousel-caption">
                            <h4>Nos Restaurants</h4>
                            <p>Découvrez nos partenaires, Optez pour une livraison directement chez eux.</p>
                            <p>Appréciez VOTRE vin durant le repas.</p></p>
                        </div>
                    </div>
                </div>
                <!-- Carousel nav -->
                <a class="carousel-control left" href="#myCarousel" data-slide="prev">&lsaquo;</a>
                <a class="carousel-control right" href="#myCarousel" data-slide="next">&rsaquo;</a>
            </div>



            <div class="inpage">
                <ul class="nav nav-tabs">
                    <li id="nouveau" class="active">
                        <a onclick="nouveau()">Nouveaux vins</a>
                    </li>
                    <li id="meilleur"><a onclick="meilleur()">Meilleurs ventes</a></li>
                    <li id="newpartner"><a onclick="partner()">Nouveaux partenaires</a></li>
                </ul>
                <div id="ongletNouveau" class="ongletcontent">
                    <% if (listeNouveaux != null && listeNouveaux.size() > 0) {
                            out.println("<ul class=\"thumbnails\">");%>
                    <%
                        int i = 0;
                        for (Iterator it = listeNouveaux.iterator(); it.hasNext() && i < 3;) {
                            Vin elem = (Vin) it.next();
                            out.println("<li class=\"span2\">");
                            out.println("<div class=\"thumbnail\">");
                            out.println("<img class=\"imgPetite\" src=\"" + elem.getImageUri() + "\"/>");
                            out.println("<div class=\"caption\">");
                            out.println("<h4>" + elem.getNom() + "</h4>");
                            out.println("<p>" + "Ref : " + elem.getId() + "</p>");
                            out.println("</div>");
                            out.println("</div>");
                            out.println("</li>");
                            i++;
                        }
                        out.println("</ul>");
                    } else {
                        out.println("<ul class=\"thumbnails\">");%>
                    <li>Pas de vin correspondant trouvé</li>
                    <%
                            out.println("</ul>");
                        }%>
                </div>
                <div id="ongletMeilleur" style="display: none;" class="ongletcontent">
                    <% if (listeMeilleurs != null && listeMeilleurs.size() > 0) {
                            out.println("<ul class=\"thumbnails\">");%>
                    <%
                        int i = 0;
                        for (Iterator it = listeMeilleurs.iterator(); it.hasNext() && i < 5;) {
                            Vin elem = (Vin) it.next();
                            out.println("<li class=\"span2\">");
                            out.println("<div class=\"thumbnail\">");
                            out.println("<img class=\"imgPetite\" src=\"" + elem.getImageUri() + "\"/>");
                            out.println("<div class=\"caption\">");
                            out.println("<h4>" + elem.getNom() + "</h4>");
                            out.println("<p>" + "Ref : " + elem.getId() + "</p>");
                            out.println("</div>");
                            out.println("</div>");
                            out.println("</li>");
                            i++;
                        }
                        out.println("</ul>");
                    } else {
                        out.println("<ul class=\"thumbnails\">");%>
                    <li>Pas de vin correspondant trouvé</li>
                    <%
                            out.println("</ul>");
                        }%>
                </div>
            </div>
            <jsp:include page="../elements/footer.jsp"/>
            <div id="basdepage"></div>
        </div>
    </body>
    <script src="/Lubyb-web/js/jquery.js"></script>
    <script src="/Lubyb-web/bootstrap/js/bootstrap.js"></script>
    <script>
        
        $('.carousel').carousel({
            interval: 9000,
            pause : "hover"
        })
        
        function meilleur(){
            $('#nouveau').removeClass("active");
            $('#newpartner').removeClass("active");
            $('#meilleur').addClass("active");
            $('#ongletMeilleur').show();
            $('#ongletNouveau').hide();
        }
        
        function nouveau(){
            $('#meilleur').removeClass("active");
            $('#newpartner').removeClass("active");
            $('#nouveau').addClass("active");
            $('#ongletNouveau').show();
            $('#ongletMeilleur').hide();
        }
        
        function partner() {
            $('#meilleur').removeClass("active");
            $('#nouveau').removeClass("active");
            $('#newpartner').addClass("active");
            $('#ongletNouveau').hide();
            $('#ongletMeilleur').hide();
        }
        
    </script>
</html>
