<%@page import="ricm5.ecom.entitybean.Restaurant"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Les Partenaires</title>
    </head>
    <body id="body">
        <% List<Restaurant> listeRestaus = (List<Restaurant>) request.getAttribute("listepartenaires");%>
        <div id="page">
            <jsp:include page="../elements/header.jsp">
                <jsp:param name="pagecourante" value="restaus"/>
            </jsp:include>
            <div>
                <jsp:include page="../elements/breadcrumbs.jsp">
                    <jsp:param name="number" value="2"/>
                    <jsp:param name="path1" value="/Lubyb-web"/>
                    <jsp:param name="number1" value="Accueil"/>
                    <jsp:param name="path2" value="/Lubyb-web/Restaurants"/>
                    <jsp:param name="number2" value="Restaurants partenaires"/>
                </jsp:include>
            </div>
            <div class="inpage">
                <h1>Nos partenaires :</h1>

                    <%
                        if (listeRestaus != null && !listeRestaus.isEmpty()) {%>
                    <table class="table table-striped">
                        <thead>
                            <tr>
                                <td>Nom</td>
                                <td>Adresse</td>
                            </tr>
                        </thead>
                        <tbody>
                            <%
                                for (Iterator it = listeRestaus.iterator(); it.hasNext();) {
                                    Restaurant restau = (Restaurant) it.next();
                            %>
                            <tr>
                                <td><% out.println(restau.getName());%></td>
                                <td><% out.println(restau.getAdresse());%></td>
                                <td><a target="_blank" href="http://maps.google.fr/maps?f=q&hl=fr&q=<% out.println(restau.getAdresse()); %>">Voir la carte</a></td>
                            </tr>
                            <%
                                }
                            %>
                        </tbody>
                    </table>
                    <%                        } else {%>
                     <div class="alert alert-info">
                     Il n'y a actuellement pas de partenaires !
                     </div>
                    <%}
                    %>
                    <a>Devenir partenaire</a>
                </div>
            <jsp:include page="../elements/footer.jsp"/>
            <div id="basdepage"></div>
        </div>
    </body>
    <script src="/Lubyb-web/js/jquery.js"></script>
</html>
