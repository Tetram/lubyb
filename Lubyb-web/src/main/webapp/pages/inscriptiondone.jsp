<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body id="body">
        <div id="page">
            <jsp:include page="../elements/header.jsp">
                <jsp:param name="pagecourante" value=""/>
            </jsp:include>
            <div>
                <jsp:include page="../elements/breadcrumbs.jsp">
                    <jsp:param name="number" value="3"/>
                    <jsp:param name="path1" value="/Lubyb-web"/>
                    <jsp:param name="number1" value="Accueil"/>
                    <jsp:param name="path2" value="/Lubyb-web/Inscription"/>
                    <jsp:param name="number2" value="Inscription"/>
                    <jsp:param name="path3" value="/Lubyb-web/Inscription"/>
                    <jsp:param name="number3" value="Inscription Terminée"/>
                </jsp:include>
            </div>
            <div class="inpage">
                Merci de vous être enregistré. Vous allez être redirigé vers la page d'accueil...
            </div>
            <jsp:include page="../elements/footer.jsp"/>
            <div id="basdepage"></div>
        </div>
    </body>
    <script src="/Lubyb-web/js/jquery.js"></script>
    <script>
        
        window.onload=timeout;
        function timeout(){
            window.setTimeout("redirect()",3000)
        }
        
        function redirect(){
            document.location.href="/Lubyb-web"
        }
    </script>
</html>
