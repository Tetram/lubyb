<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page import="ricm5.ecom.entitybean.Vin"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <%
        List<Vin> liste = (List<Vin>)request.getAttribute("liste");
        String message = (String) request.getAttribute("message");
        if(liste.size()>0){
            if(!(message.isEmpty())){
                out.println(message);
            }
            for (Iterator it = liste.iterator(); it.hasNext();) {
              Vin elem = (Vin) it.next();
              out.println(" <b> nom: "+elem.getNom()+" </b><br />");
              out.println("Ref: "+elem.getId()+"<br /> ");
              out.println("Annee: "+elem.getAnnee()+"<br /> ");
              out.println("Prix: "+elem.getPrix()+"<br /> ");
              out.println("Region: "+elem.getRegion()+"<br /> ");
              out.println("Type: "+elem.getType()+"<br /> ");
              out.println("Stock: "+elem.getStock()+"<br /> <br />");
            }
        }
               else {out.println("Aucun résultat trouvé");}
        %>
    </body>
</html>
