
<%@page import="java.util.Map"%>
<%@page import="java.util.Iterator"%>
<%@page import="ricm5.ecom.entitybean.Vin"%>
<%@page import="ricm5.ecom.entitybean.Commande"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>    
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Confirmation de commande</title>
        <link href="../bootstrap/css/bootstrap.css" rel="stylesheet">
        <link href="../css/page.css" rel="stylesheet">
    </head>
    <body id="body">
        <div id="page">
            <jsp:include page="../elements/header.jsp">
                <jsp:param name="pagecourante" value="Confirmation de commande"/>
            </jsp:include>
            <div id="breadcrumbs">
                <jsp:include page="../elements/breadcrumbs.jsp">
                    <jsp:param name="number" value="1"/>
                    <jsp:param name="path1" value="/Lubyb-web"/>
                    <jsp:param name="number1" value="Confirmation de commande"/>
                </jsp:include>
            </div>
            <%
                Commande commande = (Commande) request.getAttribute("validatedCommande");
                Long id_commande = commande.getId();
            %>
            
            <div class="inpage"> 
                <div class="alert alert-success">
                    <p><strong>Votre commande n° <%out.println(id_commande);%> a été bien prise en compte et nous vous en remercions</strong></p>
                    <p>Pour retrouver le détail de vos commandes, allez dans l'espace Mon compte/Mes commandes</p>         
                </div>
                  <div class="alert alert-help">  
                    Vous allez maintenant être redirigé vers la page d'accueil!
                  </div>
            </div>
                        
            <!--//footer-->
            <jsp:include page="../elements/footer.jsp"/>
            <div id="basdepage"></div>
            
        </div>
    </body>
        <script src="/Lubyb-web/js/jquery.js"></script>
    <script>
        
        window.onload=timeout;
        function timeout(){
            window.setTimeout("redirect()",3000)
        }
        
        function redirect(){
            document.location.href="/Lubyb-web"
        }
    </script>
</html>
