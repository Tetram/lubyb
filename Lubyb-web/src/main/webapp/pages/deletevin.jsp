<%@page import="ricm5.ecom.entitybean.Vin"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Supprimer catalogue</title>
    </head>
    <body id="body">
        <div id="page">
            <jsp:include page="../elements/headerAdmin.jsp">
                <jsp:param name="pagecourante" value=""/>
            </jsp:include>
            <div>
                <jsp:include page="../elements/breadcrumbs.jsp">
                    <jsp:param name="number" value="3"/>
                    <jsp:param name="path1" value="/Lubyb-web"/>
                    <jsp:param name="number1" value="Accueil"/>
                    <jsp:param name="path2" value="/Lubyb-web/AdminPage"/>
                    <jsp:param name="number2" value="Administration"/>
                    <jsp:param name="path3" value="/Lubyb-web/Inscription"/>
                    <jsp:param name="number3" value="Gestion des vins"/>
                </jsp:include>
            </div>

            <div class="inpage">
                <form id="deletevin" method="post">
                    <fieldset>

                        <% out.println("<form action='AddVin'>");
                            out.println("<input type='hidden' value='add' name='ref'>");
                            out.println("<input class='btn btn-primary' type='submit' value='+ Ajouter un vin'>");
                   out.println("</form>");%>

                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <td><b>Nom</b></td>
                                    <td><b>Reference</b></td>
                                    <td><b>Annee</b></td>
                                    <td><b>Prix</b></td>
                                    <td><b>Region</b></td>
                                    <td><b>Type</b></td>
                                    <td><b>Stock</b></td>
                                </tr>
                            </thead>
                            <%
                                List vins = (List) session.getAttribute("liste");
                                for (Iterator it = vins.iterator(); it.hasNext();) {
                                    out.println("<tr>");
                                    Vin elem = (Vin) it.next();
                                    out.println(" <td><b>" + elem.getNom() + " </b><br /></td>");
                                    out.println("<td>" + elem.getId() + "<br /></td> ");
                                    out.println("<td>" + elem.getAnnee() + "<br /></td> ");
                                    out.println("<td>" + elem.getPrix() + "<br /></td> ");
                                    out.println("<td>" + elem.getRegion() + "<br /></td> ");
                                    out.println("<td>" + elem.getType() + "<br /></td> ");
                                    out.println("<td>" + elem.getStock() + "<br /></td> ");
                                    out.println("<form action='DeleteVin'>");
                                    out.println("<td> <input type='hidden' value=" + elem.getId() + " name='ref'> </td> ");
                                    out.println("<td> <input id='submitForm' class='btn btn-danger' type='submit' value='supprimer' ><br/></td>");
                                    out.println("</form>");

                                    out.println("</tr>");

                }%>
                        </table>           
                    </fieldset>
                </form>
            </div>
            <jsp:include page="../elements/footer.jsp"/>
            <div id="basdepage"></div>
        </div>
    </body>
    <script src="/Lubyb-web/js/jquery.js"></script>
    <script>
        function checkForm(){
            $('#submitForm').attr('disabled', 'true');
            //Tester les champs !
            ok = true;
            
            if($('#reference').val() == '') {ok = false;}
          
            if(ok){
                $('#submitForm').removeAttr("disabled");}
        }
        
    </script>
</html>
