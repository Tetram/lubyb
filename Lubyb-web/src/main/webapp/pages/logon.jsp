<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>    
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>S'identifier</title>
        <link href="../bootstrap/css/bootstrap.css" rel="stylesheet">
        <link href="../css/page.css" rel="stylesheet">
    </head>
    <body id="body">
        <div id="page">
            <jsp:include page="../elements/header.jsp">
                <jsp:param name="pagecourante" value="S'identifier"/>
            </jsp:include>
            <div style="margin: 0em 10em 0em 15em ">
                <%
                if (request.getParameter("auth-error") != null){%>
                    <div style="margin-bottom:1em;margin-top:1em;color:red;">
                        Login ou mot de passe invalide, Veuillez r&eacute;essayer.<br>
                    </div>
                <%}
                if (request.getParameter("accessDeniedMsg") != null){%>
                    <div style="margin-bottom:1em;margin-top:1em;color:red;">
                        <%out.println(request.getParameter("accessDeniedMsg"));%>
                    </div>
                <%}%>

                <h> Laissez nous apporter vos bouteilles en vous identifiant
                    ou en créant un compte </h>
                <br><br>

                <%
                String redirect = request.getParameter("redirect");
                if ( redirect != null){%>
                    <form class="form-horizontal" action="/Lubyb-web/Authenticator?redirect=<% out.println(redirect);%>" method="post"> 
                <%}else{%>
                    <form class="form-horizontal" action="/Lubyb-web/Authenticator" method="post"> 
                <%}%>
                
                    <div class="control-group">
                      <label class="control-label"  for="inputLogin">Identifiant</label>
                      <div class="controls">
                        <input type="text" id="login" placeholder="Login" name="login">
                      </div>
                    </div>
                    <div class="control-group">
                      <label class="control-label" for="inputPassword">Mot de passe</label>
                      <div class="controls">
                        <input type="password" id="inputPassword" placeholder="Mot de passe" name="password">
                      </div>
                    </div>
                    <div class="control-group">
                      <div class="controls">
                        <label class="checkbox">
                          <input type="checkbox"> Rester connect&eacute;
                        </label>
                        <button type="submit" class="btn btn-primary">Se connecter</button>
                        <a href="/Lubyb-web/Inscription" class="btn btn-info">Créer un compte</a>
                      </div>
                        
                    </div>
                </form> 
                
            </div>
        
            <!--//footer-->
            <jsp:include page="../elements/footer.jsp"/>
            <div id="basdepage"></div>
        </div>
    </body>
</html>
