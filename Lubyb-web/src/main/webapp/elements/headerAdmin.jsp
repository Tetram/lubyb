<%@page import="ricm5.ecom.sessionbean.PanierClient"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Header </title>
        <link href="/Lubyb-web/bootstrap/css/bootstrap.css" rel="stylesheet">
        <link href="/Lubyb-web/css/page.css" rel="stylesheet">
    </head>
    <body>
        <div id="ban">
            <img src ="/Lubyb-web/images/banniere.jpg"/>
        </div>
        <div class="navbar navbar-inverse">
            <div class="navbar-inner">
                <a class="brand" href="/Lubyb-web/AdminPage">Administration</a>
                <ul class="nav">
                    <li>
                    <a href="/Lubyb-web/AdminPage/DeleteVin">Vins</a></li>
                    <li class="divider-vertical"></li>
                    <li>
                    <a href="/Lubyb-web/AdminPage/ValidationPartenariat">Demandes de partenariat</a></li>
                    <li class="divider-vertical"></li>
                    <li>
                    <a href="/Lubyb-web">Retour au site</a></li>
                </ul>
                <%
                    if (session.getAttribute("login") != null) {
                        out.println("<div class=\"navbar-form pull-right\" style=\"margin-right:5px; margin-top:10px\">");
                        out.println("Bonjour, " + session.getAttribute("login") + " ");
                        out.println("<a href=\"/Lubyb-web/MonCompte\">Mon compte</a>");
                        out.println("<a href=\"/Lubyb-web/pages/logout.jsp?redirect=/Lubyb-web \">Déconnexion</a>");
                        out.println("</div>");
                    }
                %>

            </div>
        </div>
    </body>
    <script src="/Lubyb-web/js/jquery.js"></script>
</html>
