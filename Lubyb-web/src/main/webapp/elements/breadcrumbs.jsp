<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Breadcrumbs</title>
        <link href="/Lubyb-web/bootstrap/css/bootstrap.css" rel="stylesheet">
        <link href="/Lubyb-web/css/page.css" rel="stylesheet">
    </head>
    <body>
        <%            
            int number = Integer.parseInt(request.getParameter("number"));
            int i = 1;
            out.println("<ul class=\"inpage breadcrumb\">");
            while (i < number) {
                out.println("<li>");
                out.println("<a href=\""+(request.getParameter("path" + i))+
                        "\">"+request.getParameter("number" + i)+"</a> <span class=\"divider\">/</span>");
                out.println("</li>");
                i++;
            }
            out.println("<li class=\"active\">" + request.getParameter("number" + i) + "</li>");
            out.println("</ul>");
        %>
    </body>
</html>
