<%@page import="ricm5.ecom.sessionbean.PanierClient"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
                <!--Recuperer montant du panier-->
                <%
                    PanierClient panier = (PanierClient) session.getAttribute("panier");
                    Double prixtotal = 0.0;
                    Integer nbArticles = panier.getNbArticles();
                    if(panier != null){
                        prixtotal = panier.prixTotal();
                    }
                %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Header </title>
        <link href="/Lubyb-web/bootstrap/css/bootstrap.css" rel="stylesheet">
        <link href="/Lubyb-web/css/page.css" rel="stylesheet">
    </head>
    <body>
        <div id="ban">
            <div id="loginBox" class="thumbnail" style="display: none;">
                <form action ="/Lubyb-web/Authenticator?redirect=/Lubyb-web" method="post">
                    <input type="text" name="login" placeholder="Utilisateur">
                    <input type="password" name="password" placeholder="Mot de passe"> 
                    <button type="submit" class="btn">Se connecter</button>
                </form>
                <a href="/Lubyb-web/Inscription">Créer un compte</a>
            </div>
            <a href="/Lubyb-web/"><img src ="/Lubyb-web/images/banniere.jpg"/></a>
        </div>
        <div class="navbar">
            <div class="navbar-inner">
                <ul class="nav">
                    <% String pageCourante = request.getParameter("pagecourante");
                        out.println("<li");
                        if (pageCourante != null && pageCourante.equals("accueil")) {
                            out.println("class=\"active\"");
                        }
                        out.println(">");
                    %>
                    <a href="/Lubyb-web">Accueil</a></li>
                    <li class="divider-vertical"></li>
                    <%
                        out.println("<li");
                        if (pageCourante != null && pageCourante.equals("vins")) {
                            out.println("class=\"active\"");
                        }
                        out.println(">");
                    %>
                    <a href="/Lubyb-web/Vins">Vins</a></li>
                    <li class="divider-vertical"></li>
                    <li <%
                    if (pageCourante != null && pageCourante.equals("restaus")) {
                            out.println("class=\"active\"");
                        }
                    %>><a href="/Lubyb-web/Restaurants">Restaurants</a></li>
                </ul>
                    

                <div class="navbar-form pull-right"> 
                    <button class="btn btn-mini" type="button">
                        <a href="/Lubyb-web/Panier"> 
                            <img src="/Lubyb-web/images/cart.png" alt="Panier">
                        </a>
                    </button>
                    <span style="vertical-align: bottom;">
                        ( <%out.println(nbArticles);%>)
                    </span> 
                    <span style="vertical-align: bottom; font-size:  smaller;">
                        <% out.println(prixtotal);%>&nbsp;&euro;
                    </span> 
                </div>
                <%
                    if (session.getAttribute("login") != null) {
                        out.println("<div class=\"navbar-form pull-right\" style=\"margin-right:5px; margin-top:10px\">");
                        out.println("Bonjour, " + session.getAttribute("login") + " ");
                        out.println("<a href=\"/Lubyb-web/MonCompte\">Mon compte</a>");
                        out.println("<a href=\"/Lubyb-web/pages/logout.jsp?redirect=/Lubyb-web \">Déconnexion</a>");
                        out.println("</div>");
                    } else {
                        out.println("<button id=\"connexion\" onclick=\"showConnectionBox()\" type=\"button\" class=\"btn pull-right\">Connexion</button>");
                    }
                %>

            </div>
        </div>
    </body>
    <script src="/Lubyb-web/js/jquery.js"></script>
    <script>
        
        function showConnectionBox() {
            if($('#loginBox').is(':hidden')){
                $('#loginBox').show();
                $('#connexion').addClass("active");
            }
            else {
                $('#loginBox').hide();
                $('#connexion').removeClass("active");
            }
        }
        
    </script>
</html>
