
insert into REGION (ID) value ('Bordeaux');
insert into REGION (ID) value ('Jura');
insert into REGION (ID) value ('Bourgogne');
insert into REGION (ID) value ('Alsace');
insert into REGION (ID) value ('Anjou');
insert into REGION (ID) value ('Savoie');

insert into VIN (ID,ANNEE,IMAGEURI,NOM,PRIX,STOCK,TYPE,REGION_ID) values (10000,2008,'http://www.evinite.info/media/catalog/product/cache/245/image/5e06319eda06f020e43594a9c230972d/c/h/chateau-margaux-nv_11.jpg','Chateau Margaux - Grand Vin',10,5,'ROUGE','Bordeaux');
insert into VIN (ID,ANNEE,IMAGEURI,NOM,PRIX,STOCK,TYPE,REGION_ID) values (20000,1999,'http://www.gites-jura-arbois.fr/images/vin_jaune_img_9878.jpg','Arboix',15.50,10,'JAUNE','Jura');
insert into VIN (ID,ANNEE,IMAGEURI,NOM,PRIX,STOCK,TYPE,REGION_ID) values (30000,2005,'http://www.belaliebend.com.au/images/Riesling.gif','Riesling - Bellalie Bend',12.50,10,'BLANC','Alsace');
insert into VIN (ID,ANNEE,IMAGEURI,NOM,PRIX,STOCK,TYPE,REGION_ID) values (40000,2010,'http://www.lechais.com/544-716-thickbox/cabernet-d-anjou-domaine-de-la-belle-angevine.jpg','Cabernet d''Anjou - Domaine de la belle Angevine',7.50,10,'ROSE','Anjou');
insert into VIN (ID,ANNEE,IMAGEURI,NOM,PRIX,STOCK,TYPE,REGION_ID) values (50000,2006,'http://arvanitidis-winery.gr/files/repository/20090702120355_Merlot2006.jpg','Merlot',8.50,10,'ROUGE','Bordeaux');
insert into VIN (ID,ANNEE,IMAGEURI,NOM,PRIX,STOCK,TYPE,REGION_ID) values (60000,2011,'http://media.votresommelier.com/catalog/product/cache/1/image/500x500/5e06319eda06f020e43594a9c230972d/f/i/file_216_1.jpg','Roussette de Frangy',7.50,10,'BLANC','Savoie');


insert into RESTAURANT (SIRET, ADRESSE, NAME, SOLDE, STATUS, TAUX, RESPONSABLE_LOGIN) value ('00000000000001','5 place grenette 38000 Grenoble','La bonne fourchette',0,'VALIDATED',0.2,'admin');
insert into RESTAURANT (SIRET, ADRESSE, NAME, SOLDE, STATUS, TAUX, RESPONSABLE_LOGIN) value ('00000000000002','2 place Victor Hugo 38000 Grenoble','Ma maison Farcie',0,'VALIDATED',0.2,'admin');
insert into RESTAURANT (SIRET, ADRESSE, NAME, SOLDE, STATUS, TAUX, RESPONSABLE_LOGIN) value ('00000000000003','11 rue Elisee Chatin 38100 Grenoble','Chez Aurel',0,'VALIDATED',0.2,'admin');
insert into RESTAURANT (SIRET, ADRESSE, NAME, SOLDE, STATUS, TAUX, RESPONSABLE_LOGIN) value ('00000000000004','4 place Verdun 38000 Grenoble','Pref et cture',0,'VALIDATED',0.2,'admin');
