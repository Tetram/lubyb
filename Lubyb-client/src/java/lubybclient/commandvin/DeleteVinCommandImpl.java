package lubybclient.commandvin;

import java.io.PrintStream;
import java.util.StringTokenizer;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import lubybclient.ShellCommand;
import lubybclient.ShellContext;
import ricm5.ecom.sessionbean.VinRemoteInt;

public class DeleteVinCommandImpl implements ShellCommand {

    private VinRemoteInt vinRemote;
    private ShellContext _context = null;

    public DeleteVinCommandImpl(ShellContext context) {
        _context = context;
        try {
            vinRemote = (VinRemoteInt) new InitialContext().lookup("java:global/ricm5.ecom_Lubyb-ear_ear_1.0-SNAPSHOT/Lubyb-ejb-1.0-SNAPSHOT/VinFacade");
        } catch (NamingException e) {
            // TODO : erreur de recup
            System.out.println(e.toString());
        }

    }

    public String getName() {
        return "deletevin";
    }

    public String getUsage() {
        return "deletevin [ref]";
    }

    public String getShortDescription() {
        return "Delete a wine";
    }

    public void execute(String cmdline, PrintStream out, PrintStream err) {
        StringTokenizer st = new StringTokenizer(cmdline, " ");

        // Ignore the command name.
        st.nextToken();

        if (st.countTokens() == 1) {
            vinRemote.delete(Long.parseLong(st.nextToken()));
            out.println();
        } else {
            err.println("Incorrect number of arguments");
        }
    }
}
