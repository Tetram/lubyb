package lubybclient.commandvin;

import java.io.PrintStream;
import java.util.StringTokenizer;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import lubybclient.ShellCommand;
import lubybclient.ShellContext;
import ricm5.ecom.entitybean.Region;
import ricm5.ecom.entitybean.Vin;
import ricm5.ecom.sessionbean.RegionRemote;
import ricm5.ecom.sessionbean.VinRemoteInt;

public class AddVinCommandImpl implements ShellCommand {

    private VinRemoteInt vinRemote;
    private RegionRemote regionRemote;
    private ShellContext _context = null;

    public AddVinCommandImpl(ShellContext context) {
        _context = context;
        try {
            vinRemote = (VinRemoteInt) new InitialContext().lookup("java:global/ricm5.ecom_Lubyb-ear_ear_1.0-SNAPSHOT/Lubyb-ejb-1.0-SNAPSHOT/VinFacade");
            regionRemote = (RegionRemote) new InitialContext().lookup("java:global/ricm5.ecom_Lubyb-ear_ear_1.0-SNAPSHOT/Lubyb-ejb-1.0-SNAPSHOT/RegionFacade");
        } catch (NamingException e) {
            // TODO : erreur de recup
            System.out.println(e.toString());
        }

    }

    public String getName() {
        return "addvin";
    }

    public String getUsage() {
        return "addvin [reference] [product name] [region] [annee] [prix] [couleur: rouge||rose||jaune||blanc] [stock]";
    }

    public String getShortDescription() {
        return "Add a wine";
    }

    public void execute(String cmdline, PrintStream out, PrintStream err) {
        StringTokenizer st = new StringTokenizer(cmdline, " ");

        // Ignore the command name.
        st.nextToken();

        // There should be at least 1 string.
        if (st.countTokens() >= 7) {
            Vin vinToAdd = new Vin();
            if (st.hasMoreTokens()) {
                vinToAdd.setId(Long.parseLong(st.nextToken()));
            }
            if (st.hasMoreTokens()) {
                vinToAdd.setNom(st.nextToken());
            }
            if (st.hasMoreTokens()) {
                String region = st.nextToken();
                Region regionTemp = regionRemote.find(region);
                if(regionTemp == null){
                    regionTemp = new Region();
                    regionTemp.setId(region);
                    regionRemote.add(regionTemp);
                }
                vinToAdd.setRegion(regionTemp);
            }
            if (st.hasMoreTokens()) {
                vinToAdd.setAnnee(Integer.parseInt(st.nextToken()));
            }
            if (st.hasMoreTokens()) {
                vinToAdd.setPrix(Double.parseDouble(st.nextToken()));
            }
            if (st.hasMoreTokens()) {
                vinToAdd.setType(st.nextToken());
            }
            if (st.hasMoreTokens()) {
                vinToAdd.setStock(Integer.parseInt(st.nextToken()));
            }
            vinToAdd.setImageUri("http://www.liste-vin.fr/wp-content/uploads/2009/06/chateau-fonrazade-grand-cru-300x300.jpg");
            vinRemote.add(vinToAdd);
            out.println();
        } else {
            err.println("Incorrect number of arguments");
        }
    }
}
