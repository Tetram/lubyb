package lubybclient.commandvin;

import java.io.PrintStream;
import java.util.List;
import java.util.StringTokenizer;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import lubybclient.ShellCommand;
import lubybclient.ShellContext;
import ricm5.ecom.entitybean.Vin;
import ricm5.ecom.sessionbean.VinRemoteInt;

public class PrintVinCommandImpl implements ShellCommand {

    private VinRemoteInt vinRemote;
    private ShellContext _context = null;

    public PrintVinCommandImpl(ShellContext context) {
        _context = context;
        try {
            vinRemote = (VinRemoteInt) new InitialContext().lookup("java:global/ricm5.ecom_Lubyb-ear_ear_1.0-SNAPSHOT/Lubyb-ejb-1.0-SNAPSHOT/VinFacade");
        } catch (NamingException e) {
            // TODO : erreur de recup
            System.out.println(e.toString());
        }

    }

    public String getName() {
        return "printvin";
    }

    public String getUsage() {
        return "printvin";
    }

    public String getShortDescription() {
        return "Print the lis of the wines in database";
    }

    public void execute(String cmdline, PrintStream out, PrintStream err) {
        StringTokenizer st = new StringTokenizer(cmdline, " ");

        // Ignore the command name.
        st.nextToken();

        if (st.countTokens() == 0) {

            List<Vin> liste = vinRemote.findAll();
            out.println("Vins : ");
            for (Vin e : liste) {
                out.println("\t" + e.getId()
                        +" "+e.getNom()
                        +" "+e.getRegion().getId()
                        +" "+e.getPrix()
                        +" "+e.getType()
                        +" "+e.getStock());
            }
        } else {
            err.println("Incorrect number of arguments");
        }
    }
}
