package lubybclient;

public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        (new Shell(args)).run();
    }
}
