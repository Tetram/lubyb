package lubybclient.commandRestaurant;

import java.io.PrintStream;
import java.util.StringTokenizer;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import lubybclient.ShellCommand;
import lubybclient.ShellContext;
import ricm5.ecom.entitybean.Restaurant;
import ricm5.ecom.sessionbean.RestaurantRemote;

public class ValidateRestaurantCommandImpl implements ShellCommand{
   private ShellContext _context = null;
   private RestaurantRemote restauRemote;
   // TODO : ajout utilisateur aussi !! -> faire le remote, et l'inclure ici !
   
   public ValidateRestaurantCommandImpl(ShellContext context) {
        _context = context;
        try {
            restauRemote = (RestaurantRemote) new InitialContext().lookup("java:global/ricm5.ecom_Lubyb-ear_ear_1.0-SNAPSHOT/Lubyb-ejb-1.0-SNAPSHOT/RestaurantFacade");
        } catch (NamingException e) {
            // TODO : erreur de recup
            System.out.println(e.toString());
        }

    }

    public String getName() {
        return "validaterestaurant";
    }

    public String getUsage() {
        return "validaterestaurant [siret]";
    }

    public String getShortDescription() {
        return "Put status of specified PENDING restaurant to VALIDATED";
    }

    public void execute(String cmdline, PrintStream out, PrintStream err) {
        StringTokenizer st = new StringTokenizer(cmdline, " ");

        // Ignore the command name.
        st.nextToken();

        // There should be at least 1 string.
        if (st.countTokens() >= 0) {
            if (st.hasMoreTokens()) {
                String siret = st.nextToken();
                //TODO : verifier bien 14 chiffres...
                Restaurant toValidate = restauRemote.find(Long.parseLong(siret));
                if(toValidate != null && toValidate.getStatus().equals(Restaurant.Status.PENDING)){
                    toValidate.setStatus(Restaurant.Status.VALIDATED);
                    restauRemote.add(toValidate);
                }
                else{
                    out.println("Restaurant not found or not pending");
                }
            }
        } else {
            err.println("Incorrect number of arguments");
        }
    }
}
