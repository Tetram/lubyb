package lubybclient.commandRestaurant;

import java.io.PrintStream;
import java.util.StringTokenizer;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import lubybclient.ShellCommand;
import lubybclient.ShellContext;
import ricm5.ecom.entitybean.Restaurant;
import ricm5.ecom.sessionbean.RestaurantRemote;

public class AddRestaurantCommandImpl implements ShellCommand{
   private ShellContext _context = null;
   private RestaurantRemote restauRemote;
   // TODO : ajout utilisateur aussi !! -> faire le remote, et l'inclure ici !
   
   public AddRestaurantCommandImpl(ShellContext context) {
        _context = context;
        try {
            restauRemote = (RestaurantRemote) new InitialContext().lookup("java:global/ricm5.ecom_Lubyb-ear_ear_1.0-SNAPSHOT/Lubyb-ejb-1.0-SNAPSHOT/RestaurantFacade");
        } catch (NamingException e) {
            // TODO : erreur de recup
            System.out.println(e.toString());
        }

    }

    public String getName() {
        return "addrestaurant";
    }

    public String getUsage() {
        return "addrestaurant [siret] [nom] [taux]";
    }

    public String getShortDescription() {
        return "Add a restaurant";
    }

    public void execute(String cmdline, PrintStream out, PrintStream err) {
        StringTokenizer st = new StringTokenizer(cmdline, " ");

        // Ignore the command name.
        st.nextToken();

        // There should be at least 4 string.
        if (st.countTokens() >= 3) {
            Restaurant restaurantToAdd = new Restaurant();
            if (st.hasMoreTokens()) {
                String siret = st.nextToken();
                // Verifier siret correct... 14 chiffres...
                restaurantToAdd.setSiret(siret);
            }
            if(st.hasMoreTokens()){
                restaurantToAdd.setName(st.nextToken());
            }
            if (st.hasMoreTokens()) {
                restaurantToAdd.setTaux(Integer.parseInt(st.nextToken()));
            }
            restaurantToAdd.setAdresse("");
            restaurantToAdd.setSolde(0);
            restaurantToAdd.setStatus(Restaurant.Status.PENDING);
            restaurantToAdd.setResponsable(null);
            restauRemote.add(restaurantToAdd);
        } else {
            err.println("Incorrect number of arguments");
        }
    }
}
