package lubybclient.commandRestaurant;

// Commande permettant d'avoir la liste des restaurants en attente de valdiation
import java.io.PrintStream;
import java.util.List;
import java.util.StringTokenizer;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import lubybclient.ShellCommand;
import lubybclient.ShellContext;
import ricm5.ecom.entitybean.Restaurant;
import ricm5.ecom.sessionbean.RestaurantRemote;

public class ListPendingCommandImpl implements ShellCommand {

    private ShellContext _context = null;
    private RestaurantRemote restauRemote;

    public ListPendingCommandImpl(ShellContext context) {
        _context = context;
        try {
            restauRemote = (RestaurantRemote) new InitialContext().lookup("java:global/ricm5.ecom_Lubyb-ear_ear_1.0-SNAPSHOT/Lubyb-ejb-1.0-SNAPSHOT/RestaurantFacade");
        } catch (NamingException e) {
            // TODO : erreur de recup
            System.out.println(e.toString());
        }

    }

    public String getName() {
        return "listpendingrestaurants";
    }

    public String getUsage() {
        return "listpendingrestaurants";
    }

    public String getShortDescription() {
        return "Show the list of restaurants waiting for acceptation";
    }

    public void execute(String cmdline, PrintStream out, PrintStream err) {
        StringTokenizer st = new StringTokenizer(cmdline, " ");

        // Ignore the command name.
        st.nextToken();

        // There should be no string.
        if (st.countTokens() == 0) {
            List<Restaurant> liste = restauRemote.findByStatus(Restaurant.Status.PENDING);
            if (liste != null && !liste.isEmpty()) {
                out.println("Restaurants en attente de validation : ");
                for (Restaurant e : liste) {
                    String sortie = "\t" + e.getSiret();
                    if(e.getName() != null){
                        sortie += "\t"+e.getName();
                    }
                    else{
                       sortie += "\tname missing"; 
                    }
                    if(e.getAdresse() != null && !e.getAdresse().equals("")){
                        sortie += "\t"+e.getAdresse();
                    }
                    else{
                       sortie += "\taddress missing"; 
                    }
                    if(e.getName() != null){
                        sortie += "\t"+e.getTaux();
                    }
                    else{
                       sortie += "\ttaux missing"; 
                    }
                    if(e.getResponsable() != null){
                        sortie += "\t"+e.getResponsable().getLogin();
                    }
                    else{
                       sortie += "\tresponsable missing"; 
                    }
                    out.println(sortie);
                }
                out.println();
            }
            else{
                out.println("Aucun restaurant en attente");
            }
        } else {
            err.println("Incorrect number of arguments");
        }
    }
}
