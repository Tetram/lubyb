package lubybclient.commandRegion;

import java.io.PrintStream;
import java.util.List;
import java.util.StringTokenizer;
import javax.ejb.EJBException;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import lubybclient.ShellCommand;
import lubybclient.ShellContext;
import ricm5.ecom.entitybean.Region;
import ricm5.ecom.sessionbean.RegionRemote;

public class PrintRegionCommandImpl implements ShellCommand {

    private RegionRemote regionRemote;
    private ShellContext _context = null;

    public PrintRegionCommandImpl(ShellContext context) {
        _context = context;
        try {
            regionRemote = (RegionRemote) new InitialContext().lookup("java:global/ricm5.ecom_Lubyb-ear_ear_1.0-SNAPSHOT/Lubyb-ejb-1.0-SNAPSHOT/RegionFacade");
        } catch (NamingException e) {
            // TODO : erreur de recup
            System.out.println(e.toString());
        }

    }

    public String getName() {
        return "printregion";
    }

    public String getUsage() {
        return "printregion";
    }

    public String getShortDescription() {
        return "Show all regions in database";
    }

    public void execute(String cmdline, PrintStream out, PrintStream err) {
        StringTokenizer st = new StringTokenizer(cmdline, " ");

        // Ignore the command name.
        st.nextToken();

        // There should be no arg.
        if (st.countTokens() == 0) {
            List<Region> liste = regionRemote.findAll();
            out.println("Regions : ");
            for (Region e : liste) {
                out.println("\t" + e.getId());
            }
            out.println();
        } else {
            err.println("Incorrect number of arguments");
        }
    }
}
