package lubybclient.commandRegion;

import java.io.PrintStream;
import java.util.StringTokenizer;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import lubybclient.ShellCommand;
import lubybclient.ShellContext;
import ricm5.ecom.entitybean.Region;
import ricm5.ecom.sessionbean.RegionRemote;

public class AddRegionCommandImpl implements ShellCommand {

    private RegionRemote regionRemote;
    private ShellContext _context = null;

    public AddRegionCommandImpl(ShellContext context) {
        _context = context;
        try {
            regionRemote = (RegionRemote) new InitialContext().lookup("java:global/ricm5.ecom_Lubyb-ear_ear_1.0-SNAPSHOT/Lubyb-ejb-1.0-SNAPSHOT/RegionFacade");
        } catch (NamingException e) {
            // TODO : erreur de recup
            System.out.println(e.toString());
        }

    }

    public String getName() {
        return "addregion";
    }

    public String getUsage() {
        return "addregion [nom]";
    }

    public String getShortDescription() {
        return "Add a region";
    }

    public void execute(String cmdline, PrintStream out, PrintStream err) {
        StringTokenizer st = new StringTokenizer(cmdline, " ");

        // Ignore the command name.
        st.nextToken();

        // There should be at least 1 string.
        if (st.countTokens() >= 0) {
            Region regionToAdd = new Region();
            if (st.hasMoreTokens()) {
                regionToAdd.setId(st.nextToken());
            }
            regionRemote.add(regionToAdd);
            out.println();
        } else {
            err.println("Incorrect number of arguments");
        }
    }
}
