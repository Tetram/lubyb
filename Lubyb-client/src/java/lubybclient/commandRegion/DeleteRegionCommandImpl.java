package lubybclient.commandRegion;

import java.io.PrintStream;
import java.util.StringTokenizer;
import javax.ejb.EJBException;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import lubybclient.ShellCommand;
import lubybclient.ShellContext;
import ricm5.ecom.sessionbean.RegionRemote;

public class DeleteRegionCommandImpl implements ShellCommand {

    private RegionRemote regionRemote;
    private ShellContext _context = null;

    public DeleteRegionCommandImpl(ShellContext context) {
        _context = context;
        try {
            regionRemote = (RegionRemote) new InitialContext().lookup("java:global/ricm5.ecom_Lubyb-ear_ear_1.0-SNAPSHOT/Lubyb-ejb-1.0-SNAPSHOT/RegionFacade");
        } catch (NamingException e) {
            // TODO : erreur de recup
            System.out.println(e.toString());
        }

    }

    public String getName() {
        return "deleteregion";
    }

    public String getUsage() {
        return "deleteregion [nom]";
    }

    public String getShortDescription() {
        return "Delete a region";
    }

    public void execute(String cmdline, PrintStream out, PrintStream err) {
        StringTokenizer st = new StringTokenizer(cmdline, " ");

        // Ignore the command name.
        st.nextToken();

        // There should be at least 1 string.
        if (st.countTokens() >= 0) {
            String toDelete = null;
            if (st.hasMoreTokens()) {
                toDelete = st.nextToken();
            }
            try{
                regionRemote.delete(toDelete);
            }
            catch(EJBException e){
                err.println("Erreur lors de la suppression de la région ! Verifiez les vins !");
            }
            out.println();
        } else {
            err.println("Incorrect number of arguments");
        }
    }
}
