
package ricm5.ecom.lubybclient;

import java.io.PrintStream;
import java.util.StringTokenizer;
import javax.ejb.EJB;
import ricm5.ecom.entitybean.Vin;
import ricm5.ecom.sessionbean.VinFacade;

public class AddVinCommandImpl implements ShellCommand {
    
    @EJB
    private static VinFacade vinFacade;
    private ShellContext _context = null;
    
    public AddVinCommandImpl(ShellContext context) {
           _context = context;
    }

    public String getName() {
        return "addvin";
    }

    public String getUsage() {
        return "addvin [reference] [product name] [] []";
    }

    public String getShortDescription() {
        return "Add a wine";
    }

    public void execute(String cmdline, PrintStream out, PrintStream err) {
        StringTokenizer st = new StringTokenizer(cmdline, " ");

        // Ignore the command name.
        st.nextToken();

        // There should be at least 1 string.
        if (st.countTokens() >= 0)
        {
            Vin vinToAdd = new Vin();
            while (st.hasMoreTokens())
            {
                vinToAdd.setId(Long.parseLong(st.nextToken()));
            }
	    out.println();
        }
        else
        {
            err.println("Incorrect number of arguments");
        }
    }
    
}
