package ricm5.ecom.lubybclient;

/**
 * Enterprise Application Client main class.
 *
 */
public class Main {
    
    public static void main( String[] args ) {
        System.out.println( "Hello World Enterprise Application Client!" );
        new Shell(args).run();
    }
}
